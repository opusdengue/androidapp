package app.dengue.opus.les.br.denguereport.shownotification;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import app.dengue.opus.les.br.denguereport.R;
import br.les.opus.dengue.sdk.domain.PointOfInterest;

/**
 * Created by leonardo on 6/16/15.
 */
public class DengueInfoWindowAdapter implements GoogleMap.InfoWindowAdapter{
    private LayoutInflater inflater;

    public DengueInfoWindowAdapter(LayoutInflater inflater){
        this.inflater = inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View info = inflater.inflate(R.layout.info_window_layout, null);
        TextView title, desc;

        title = (TextView) info.findViewById(R.id.info_tittle);
        desc = (TextView) info.findViewById(R.id.info_desc);

        title.setText(marker.getTitle());
        desc.setText(marker.getSnippet());

        return info;
    }
}
