package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.animation.FloatingHintEditText;

/**
 * Created by leonardo on 7/21/15.
 */
public class VariableFieldDiagnosedDengueFragment extends VariableFieldFragment{
    private TextView firstView;
    private TextView secondView;
    private Spinner firstSpinner;
    private Spinner secondSpinner;
    private FloatingHintEditText editText;

    private FieldParcelable firstParcelable;
    private FieldParcelable secondParcelable;
    private FieldParcelable thirdParcelable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);

        Bundle bundleArg = getArguments();

        PoiTypeParcelable poiTypeParcelable = bundleArg.getParcelable(EXTRA);

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.variable_field_diagnosed_dengue, container, false);

        inflateWidgets(rootView, poiTypeParcelable);

        return rootView;
    }

    @Override
    public void inflateWidgets(View rootView, PoiTypeParcelable poiTypeParcelable){
        firstParcelable = poiTypeParcelable.getFieldParcelable().get(0);
        secondParcelable = poiTypeParcelable.getFieldParcelable().get(1);
        thirdParcelable = poiTypeParcelable.getFieldParcelable().get(2);

        firstView = (TextView) rootView.findViewById(R.id.firsttextview);
        secondView = (TextView) rootView.findViewById(R.id.secondtextview);

        firstView.setText(firstParcelable.getName());
        secondView.setText(secondParcelable.getName());


        firstSpinner = (Spinner) rootView.findViewById(R.id.firstspinner);
        secondSpinner = (Spinner) rootView.findViewById(R.id.secondspinner);

        editText = (FloatingHintEditText) rootView.findViewById(R.id.editTextAge);
        editText.setHint(thirdParcelable.getName());

        populateSpinnerTypeNotifications(firstSpinner, firstParcelable.getFieldOptionsParcelable());
        populateSpinnerTypeNotifications(secondSpinner, secondParcelable.getFieldOptionsParcelable());

    }

    @Override
    public void populateSpinnerTypeNotifications(Spinner spinner, List<FieldOptionParcelable> fieldOptions){

        ArrayList<String> spinnerValues = new ArrayList<>();

        for(FieldOptionParcelable option: fieldOptions){
            spinnerValues.add(option.getValue());
        }

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(
                this.getActivity(), android.R.layout.simple_spinner_item, spinnerValues);

        typeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner.setAdapter(typeAdapter);
    }

    @Override
    public List<FieldValueParcelable> getFields() {
        ArrayList<FieldValueParcelable> arrayFieldValues = new ArrayList<>();

        FieldValueParcelable firstFieldValue = new FieldValueParcelable(
                (String)firstSpinner.getSelectedItem(),
                firstParcelable.getId(),
                firstParcelable.getName()
        );

        FieldValueParcelable secondFieldValue = new FieldValueParcelable(
                (String)secondSpinner.getSelectedItem(),
                secondParcelable.getId(),
                secondParcelable.getName()
        );

        FieldValueParcelable thirdFieldValue = new FieldValueParcelable(
                editText.getText().toString(),
                thirdParcelable.getId(),
                thirdParcelable.getName());

        arrayFieldValues.add(firstFieldValue);
        arrayFieldValues.add(secondFieldValue);
        arrayFieldValues.add(thirdFieldValue);

        return arrayFieldValues;
    }
}
