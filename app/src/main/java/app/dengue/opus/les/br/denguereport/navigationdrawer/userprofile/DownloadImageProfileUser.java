package app.dengue.opus.les.br.denguereport.navigationdrawer.userprofile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by leonardo on 11/25/15.
 */
public class DownloadImageProfileUser extends AsyncTask<String, Void, Bitmap>{
    private ImageView imageView;

    public DownloadImageProfileUser(ImageView imageView) {
        this.imageView = imageView;
    }

    protected Bitmap doInBackground(String... sLink) {
        Bitmap bitmap = null;
        try {
            URI uri = new URL(sLink[0]).toURI();
            InputStream in = uri.toURL().openStream();
            bitmap = BitmapFactory.decodeStream(in);
        } catch (URISyntaxException | MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    protected void onPostExecute(Bitmap result) {
        imageView.setImageBitmap(result);
    }
}
