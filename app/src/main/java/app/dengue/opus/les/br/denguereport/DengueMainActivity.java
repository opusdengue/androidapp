package app.dengue.opus.les.br.denguereport;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;


import java.util.ArrayList;

import app.dengue.opus.les.br.denguereport.authentication.Authentication;
import app.dengue.opus.les.br.denguereport.authentication.AuthenticationUser;
import app.dengue.opus.les.br.denguereport.authentication.FacebookUser;
import app.dengue.opus.les.br.denguereport.capturenotification.CaptureNotificationActivity;
import app.dengue.opus.les.br.denguereport.capturenotification.camera.TakePicture;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PictureParcelable;
import app.dengue.opus.les.br.denguereport.config.StorePreferences;
import app.dengue.opus.les.br.denguereport.config.UserPreferences;
import app.dengue.opus.les.br.denguereport.navigationdrawer.ItemDivider;
import app.dengue.opus.les.br.denguereport.navigationdrawer.userprofile.MyAdapter;
import app.dengue.opus.les.br.denguereport.navigationdrawer.userprofile.NavigationDrawerData;
import app.dengue.opus.les.br.denguereport.serverHandler.SendPoiIntentService;
import app.dengue.opus.les.br.denguereport.shownotification.DengueInfoWindowAdapter;
import app.dengue.opus.les.br.denguereport.shownotification.PlotMarker;
import app.dengue.opus.les.br.denguereport.shownotification.SaveAsyncTaskFragment;
import app.dengue.opus.les.br.denguereport.util.AlertDialogInterface;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;
import app.dengue.opus.les.br.denguereport.util.MapFilterAddress.DelayAutoCompleteTextView;
import app.dengue.opus.les.br.denguereport.util.MapFilterAddress.GeoAutoCompleteAdapter;
import app.dengue.opus.les.br.denguereport.util.MapFilterAddress.GeoSearchResult;
import app.dengue.opus.les.br.denguereport.util.MapFilterAddress.SearchAutoCompleteAdapter;
import app.dengue.opus.les.br.denguereport.util.RetainMapFragment;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.domain.PointOfInterest;

@TargetApi(21)
public class DengueMainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, AppCompatCallback {
    private FacebookUser facebookUser;
    private UserPreferences userPreferences;

    private GoogleApiClient googleApiClient;
    private GoogleMap map;
    private Location mLastLocation;
    private PoiSendReceiver receiver;
    private SaveAsyncTaskFragment saveAsyncTaskFragment;


    private RecyclerView mRecyclerView;                           // Declaring RecyclerView
    private MyAdapter mAdapter;                        // Declaring Adapter For Recycler View
    private RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    public static DrawerLayout drawer;                            // Declaring DrawerLayout
    private int[] iconsDrawer;                                    // icons
    private NavigationDrawerData drawerData;
    private ImageButton buttonAction;

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar drawer Toggle

    private Integer THRESHOLD = 2;
    private DelayAutoCompleteTextView geo_autocomplete;
    //private ImageView geo_autocomplete_clear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkInternetConnectivity();

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.drawer_layout);


        //create (or load fragment) to maintain the asynctask alive during configuration change
        if(savedInstanceState == null){
            saveAsyncTaskFragment = new SaveAsyncTaskFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(saveAsyncTaskFragment, SaveAsyncTaskFragment.IDENTIFIER)
                    .commit();
        }else{
            saveAsyncTaskFragment = (SaveAsyncTaskFragment) getSupportFragmentManager()
                    .findFragmentByTag(SaveAsyncTaskFragment.IDENTIFIER);
        }

        addUiComponents();
        loadPreferences();

        buildGoogleApiClient();


        checkGPSEnableToSendNotification(null);



    }

    private void checkInternetConnectivity(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            DialogAlert.displayDialog(
                    this,
                    getString(R.string.app_name),
                    getString(R.string.internet_connection),
                    getString(R.string.yes_button),
                    getString(R.string.cancel_button),
                    true,
                    new AlertDialogInterface() {
                        @Override
                        public void onButtonClicked(boolean value) {
                            if (value) {
                                Intent callInternetSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_WIFI_SETTINGS);

                                startActivity(callInternetSettingIntent);
                            }
                        }
                    });
        }
    }


    public GoogleMap getMap() {
        return map;
    }


    private void addUiComponents() {
        RetainMapFragment mapFragment = (RetainMapFragment) getFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(new PlaceholderFragment());
        map = mapFragment.getMap();
        map.setInfoWindowAdapter(new DengueInfoWindowAdapter(getLayoutInflater()));


        //String name = getString(R.string.anonymous_user);
        String name = getString(R.string.app_name);
        String email = "";
        int profile = R.drawable.ic_anonymous_user;

        Resources res = getResources();
        TypedArray ar = getResources().obtainTypedArray(R.array.icons_navigation_drawer);
        String[] titles = res.getStringArray(R.array.items_navigation_drawer);

        int len = ar.length();

        iconsDrawer = new int[len];

        for (int i = 0; i < len; i++) {
            iconsDrawer[i] = ar.getResourceId(i, 0);
        }


        ar.recycle();

        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        // RecyclerView ItemDecoration (divider)
        final RecyclerView.ItemDecoration itemDecoration = new ItemDivider(this);
        mRecyclerView.addItemDecoration(itemDecoration);


        drawerData = new NavigationDrawerData(titles
                , iconsDrawer, name, email, profile);
        mAdapter = new MyAdapter(drawerData);


        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView


        final GestureDetector myGestureDetector = new GestureDetector(DengueMainActivity.this,
                new GestureDetector.SimpleOnGestureListener(){
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

        mRecyclerView.addOnItemTouchListener(new HandleItemTouchListener(myGestureDetector));


        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer,toolbar,R.string.drawer_open,R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        };

        // drawer Toggle Object Made
        drawer.setDrawerListener(mDrawerToggle); // drawer Listener set to the drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
        drawer.openDrawer(Gravity.LEFT);


        buttonAction = (ImageButton) findViewById(R.id.addNotifyButton);


        if(saveAsyncTaskFragment.getActivated()){
            buttonAction.setImageResource(R.drawable.ic_back);
        }

    }

    private void loadPreferences() {
        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        userPreferences = StorePreferences.getUserPreferences(sharedPreferences);

        //check to see whether the user is connected or not
        if(userPreferences.isConnected()){
            //user is connected, get his configuration

            facebookUser = new FacebookUser();

            // If the access token is available already assign it.
            facebookUser.setAccessToken(AccessToken.getCurrentAccessToken());


            facebookUser.setProfile(Profile.getCurrentProfile());
            facebookUser.setUrlCover(userPreferences.getExtra());


            drawerData.changeTitleToLoginOrLogout(getString(R.string.logout_string));
            mAdapter = new MyAdapter(drawerData);
            mAdapter.setFacebookUser(facebookUser);


            mAdapter.notifyDataSetChanged();
            mRecyclerView.setAdapter(mAdapter);

        }

    }

    private void savePreferences(){
        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        StorePreferences.storePreferences(sharedPreferences, userPreferences);
    }


    //this method is called when the app cannot access the server
    public void displayErrorInfo(){
        DialogAlert.displayInfoDialog(this,
                this.getString(R.string.error_message),
                this.getString(R.string.server_unavailable),
                this.getString(R.string.dismiss)
        );

        //return the button to the default icon
        buttonAction.setImageResource(R.drawable.ic_action);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.searchview).getActionView();

        searchView.setOnQueryTextListener(new SearchAutoCompleteAdapter(this, searchView));

        searchView.clearFocus();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(this, AboutAppActivity.class);
                startActivity(intent);
                return true;

        }


        return super.onOptionsItemSelected(item);

    }

    /**
     * Create an instance of the Google Play services API client.
     * Required to use location services
     */
    protected synchronized void buildGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

       googleApiClient.connect();

    }

    @Override
    public void onConnected(Bundle bundle) {
        if((mLastLocation == null) && (saveAsyncTaskFragment.getActivated() == false)){
            setLocation();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onDestroy(){
        savePreferences();

        super.onDestroy();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceSate has occurred.
        //dlDrawer.getDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
       // dlDrawer.getDrawerToggle().onConfigurationChanged(newConfig);
    }

    //Method that check wheter the gps is enable
    public void checkGPSEnableToSendNotification(View v){
        boolean gps = checkGPS();

        if (!gps){
            showGPSDisabledAlertToUser();
        }else{
            setLocation();
        }

    }


    public boolean checkGPS(){
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    private void setLocation(){
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);

        if (mLastLocation != null) {
            moveCameraToMap(mLastLocation);
        }
    }



    public void moveCameraToMap(Location location){
        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

        CameraPosition cameraPostion = new CameraPosition.Builder()
                .target(latLng).zoom(15).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPostion));

    }



    //Create alert to user to enable gps
    private void showGPSDisabledAlertToUser() {
        DialogAlert.displayDialog(
                this,
                null,
                getString(R.string.gps_disabled),
                getString(R.string.goto_settings_page),
                getString(R.string.cancel_button),
                true,
                new AlertDialogInterface() {
                    @Override
                    public void onButtonClicked(boolean value) {
                        if (value) {
                            Intent callGPSSettingIntent = new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(callGPSSettingIntent, RESULT_FIRST_USER);
                        } else {
                            finish();
                        }
                    }
                });
    }



    //Method that call the Take Picture Activity
    public void callTakePictureActivity(View v){
        /**
         * true means the map is showing markers, so the operation should be
         * to clean the map
         * 
         * false means the operation is to capture the notification
         */

        if(saveAsyncTaskFragment.getActivated()){
            map.clear();
            saveAsyncTaskFragment.cancelToast();
            saveAsyncTaskFragment.setActivated(false);
            buttonAction.setImageResource(R.drawable.ic_action);
        }else{
            //call take picture and start the operation to capture a notification
            if(checkGPS()){

                DialogAlert.displayDialog(this,
                        getString(R.string.notification),
                        getString(R.string.notification_info),
                        getString(R.string.yes_button),
                        getString(R.string.no_button),
                        true,
                        new AlertDialogInterface() {
                            @Override
                            public void onButtonClicked(boolean value) {
                                if (value) {
                                    Intent intent = new Intent(DengueMainActivity.this, TakePicture.class);
                                    startActivityForResult(intent, RESULT_FIRST_USER);
                                } else {
                                    Intent intent = new Intent(DengueMainActivity.this, CaptureNotificationActivity.class);
                                    intent.putExtra(TakePicture.NOPICTURE, true);
                                    startActivityForResult(intent, RESULT_FIRST_USER);
                                }
                            }
                        });



            }else{
                Toast.makeText(this, getString(R.string.toast_gps_disabled), Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //result for to the capture of notification. Next step is send it to server
        if(requestCode == RESULT_FIRST_USER && resultCode == RESULT_OK){


            TemporaryPoi temporaryPoi = data.getParcelableExtra(SendPoiIntentService.POI);

            Toast.makeText(DengueMainActivity.this, getString(R.string.toast_send_poi), Toast.LENGTH_SHORT).show();

            Intent ptIntent = new Intent(this, SendPoiIntentService.class);

            String sPicture = data.getStringExtra(SendPoiIntentService.PICTURE);

            //the intent has a picture url
            if(sPicture != null){
                ArrayList<PictureParcelable> pictureParcelables = new ArrayList<>();
                pictureParcelables.add(new PictureParcelable(sPicture));
                temporaryPoi.setPictures(pictureParcelables);
            }


            ptIntent.putExtra(SendPoiIntentService.POI, temporaryPoi);
            ptIntent.putExtra(SendPoiIntentService.LOCATION, mLastLocation);

            startService(ptIntent);

            IntentFilter filter = new IntentFilter(SendPoiIntentService.RESPONSE_STRING);
            filter.addCategory(Intent.CATEGORY_DEFAULT);
            receiver = new PoiSendReceiver();
            registerReceiver(receiver, filter);

        }

        //result for to the activation of GPS
        if(requestCode == RESULT_FIRST_USER && resultCode == RESULT_CANCELED){
            setLocation();
        }


        //result for the facebook login
        if(requestCode == RESULT_FIRST_USER && resultCode == RESULT_FIRST_USER){
            facebookUser = data.getParcelableExtra(AuthenticationUser.RESULT);

            //check to see if the result is login our logout
            //check to see if the user is connected or not
            if(facebookUser!= null && facebookUser.isConnected()){
                drawerData.changeTitleToLoginOrLogout(getString(R.string.logout_string));
                mAdapter = new MyAdapter(drawerData);
                mAdapter.setFacebookUser(facebookUser);

                userPreferences.setConnected(true);
                userPreferences.setExtra(facebookUser.getUrlCover());

            }else{
                drawerData.changeTitleToLoginOrLogout(getString(R.string.login_string));
                mAdapter = new MyAdapter(drawerData);
                mAdapter.setFacebookUser(null);

                userPreferences.setConnected(false);
                userPreferences.setExtra(null);
            }

            mAdapter.notifyDataSetChanged();
            mRecyclerView.setAdapter(mAdapter);
            savePreferences();
        }

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements OnMapReadyCallback {

        public PlaceholderFragment() {

        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_dengue_main, container, false);
            return rootView;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    /**
     * Class that receive the poi after he has been sent to server
     */
    public class PoiSendReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean status = intent.getBooleanExtra(SendPoiIntentService.STATUS, false);

            if(status){
                Location location = intent.getParcelableExtra(SendPoiIntentService.LOCATION);
                TemporaryPoi temporaryPoi = intent.getParcelableExtra(SendPoiIntentService.POI);

                Picture picture = null;

                if(temporaryPoi.getPictures().size() > 0){
                    picture = new Picture();
                    picture.setId(temporaryPoi.getPictures().get(0).getId());
                }


                PointOfInterest pointOfInterest = TemporaryPoi.convertToPointOfInterest(
                        temporaryPoi,
                        picture,
                        location
                );

                pointOfInterest.setId(temporaryPoi.getId());


                PlotMarker marker = new PlotMarker(map, DengueMainActivity.this);

                marker.plotPointOfInterest(pointOfInterest);

                moveCameraToMap(location);

                Toast.makeText(DengueMainActivity.this, R.string.notification_sent, Toast.LENGTH_SHORT).show();

                unregisterReceiver(this);

                saveAsyncTaskFragment.setActivated(true);

                buttonAction.setImageResource(R.drawable.ic_back);
            }else{
                DialogAlert.displayDialog(
                        context,
                        getString(R.string.error_message),
                        getString(R.string.server_unreacheable),
                        getString(R.string.yes_button),
                        getString(R.string.cancel_button),
                        true,
                        new AlertDialogInterface() {
                            @Override
                            public void onButtonClicked(boolean value) {
                                if(value){
                                    //implement

                                }else{
                                    //implement
                                }
                            }
                        });
            }

        }
    }

    /**
     * class that handle with click events of the drawer iconsDrawer
     */
    private class HandleItemTouchListener implements RecyclerView.OnItemTouchListener{
        private final GestureDetector gestureDetector;

        public HandleItemTouchListener(GestureDetector gestureDetector){
            this.gestureDetector = gestureDetector;
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = mRecyclerView.findChildViewUnder(e.getX(), e.getY());

            if(child != null && gestureDetector.onTouchEvent(e)){
                drawer.closeDrawers();
                checkIcon(child);
                return true;
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

        private void checkIcon(View child){
            int adapterPosition = mRecyclerView.getChildAdapterPosition(child);
            if(adapterPosition < 1){
                return;
            }

            //clean the map before to plotPointOfInterests others notifications
            if(saveAsyncTaskFragment.getActivated()){
                map.clear();
            }


            //identifies the clicked button and takes the righ action
            switch (iconsDrawer[adapterPosition - 1]){
                case R.drawable.ic_action:
                    callTakePictureActivity(null);
                    break;

                case R.drawable.ic_login:
                    //perform the authentication
                    Intent intent = new Intent(DengueMainActivity.this, Authentication.class);

                    //if the user is connected, then means that it's time to logout
                    if(facebookUser != null && facebookUser.isConnected()){
                        intent.putExtra(AuthenticationUser.LOGOUT,true);
                    }

                    startActivityForResult(intent, RESULT_FIRST_USER);

                    break;

                case R.drawable.ic_notification:
                    saveAsyncTaskFragment.startTaskToGetPoi();
                    saveAsyncTaskFragment.setActivated(true);

                    buttonAction.setImageResource(R.drawable.ic_back);
                    break;

                case R.drawable.ic_twitter:

                    saveAsyncTaskFragment.startTaskToGetTweets();
                    saveAsyncTaskFragment.setActivated(true);
                    buttonAction.setImageResource(R.drawable.ic_back);

                    break;

                case R.drawable.ic_instagram:
                    saveAsyncTaskFragment.startTaskToGetInstagram();
                    saveAsyncTaskFragment.setActivated(true);
                    buttonAction.setImageResource(R.drawable.ic_back);
                    break;

                case R.drawable.ic_heatmap:
                    saveAsyncTaskFragment.startTaskToGetHeatMap();
                    saveAsyncTaskFragment.setActivated(true);
                    buttonAction.setImageResource(R.drawable.ic_back);
                    break;

                case R.drawable.ic_search:

                    break;

                default:
                    Toast.makeText(DengueMainActivity.this, "Not implemented yet ", Toast.LENGTH_SHORT).show();
            }

        }

    }

}
