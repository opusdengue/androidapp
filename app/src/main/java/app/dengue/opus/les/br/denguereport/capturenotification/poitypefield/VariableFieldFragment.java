package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.app.Fragment;
import android.view.View;
import android.widget.Spinner;

import java.util.List;

/**
 * Created by leonardo on 7/22/15.
 */
public abstract class VariableFieldFragment extends Fragment{

    public static final String EXTRA = "VariableFieldFragment.EXTRA";

    public abstract void inflateWidgets(View rootView, PoiTypeParcelable poiTypeParcelable);
    public abstract void populateSpinnerTypeNotifications(Spinner spinner, List<FieldOptionParcelable> fieldOptions);

    public abstract List<FieldValueParcelable> getFields();
}
