package app.dengue.opus.les.br.denguereport.navigationdrawer.userprofile;

/**
 * Created by leonardo on 11/27/15.
 * This class contains arrays to store icons, titles. It also has data related to the profile ui
 */
public class NavigationDrawerData {
    private String titles[];
    private int icons[];
    private String name;
    private String email;
    private int idProfile;

    public NavigationDrawerData(String[] titles, int[] icons, String name, String email, int idProfile) {
        this.titles = titles;
        this.icons = icons;
        this.name = name;
        this.email = email;
        this.idProfile = idProfile;
    }

    public String[] getTitles() {
        return titles;
    }

    public void setTitles(String[] titles) {
        this.titles = titles;
    }

    public int[] getIcons() {
        return icons;
    }

    public void setIcons(int[] icons) {
        this.icons = icons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    public void changeTitleToLoginOrLogout(String sLoginLogout) {
        titles[titles.length - 1] = sLoginLogout;
    }
}
