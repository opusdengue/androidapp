package app.dengue.opus.les.br.denguereport.shownotification.markerdetails;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;

import app.dengue.opus.les.br.denguereport.AboutAppActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.util.TweetParcelable;
import br.les.opus.dengue.sdk.domain.PointOfInterest;

/**
 * Created by leonardo on 10/12/15.
 */
public class ShowTweetDetails extends AppCompatActivity implements MarkerDetails{
    private TweetParcelable tweetParcelable;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tweet_details_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tweetParcelable = getIntent().getParcelableExtra(EXTRA);

        imageView = (ImageView) findViewById(R.id.imageViewUser);

        if(tweetParcelable != null){
            addUIComponents();
        }


    }
    @Override
    public void addUIComponents(PointOfInterest poi){

    }

    //populate the components with the tweet info

    public void addUIComponents(){
        TextView tvUserName = (TextView) findViewById(R.id.textViewUserName);
        TextView tvProfile = (TextView) findViewById(R.id.textViewUser);
        TextView tvTweetDescription = (TextView) findViewById(R.id.textViewDescription);
        TextView tvTweetClassification = (TextView) findViewById(R.id.textViewClassification);
        TextView tvTweetDate = (TextView) findViewById(R.id.textViewDate);

        tvUserName.setText(tweetParcelable.getUser().getUser());
        tvProfile.setText("@" + tweetParcelable.getUser().getProfile());
        tvTweetDescription.setText(tweetParcelable.getText());

        tvTweetClassification.setText(tweetParcelable.getClassification().getDescription());

        DateFormat dateFormat = DateFormat.getDateTimeInstance();

        tvTweetDate.setText(dateFormat.format(tweetParcelable.getCreatedAt()));

        String urlProfilePicture = getUrlProfileNormalSize(tweetParcelable.getUser().getUrlProfile());

        /*
        * try to download the image. If the download fail, the default image is put in its place
        */
        new DownloadImage().execute(urlProfilePicture);

    }


    //Test the url, if it's a thumbnail, change the url to get the normal size
    private String getUrlProfileNormalSize(String urlProfilePicture) {
        if(urlProfilePicture.contains("_normal.")) {
            urlProfilePicture = urlProfilePicture.replace("_normal.", ".");
        }
        return urlProfilePicture;
    }


    private void setImage(Bitmap image){
        imageView.setImageBitmap(image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dengue_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(this, AboutAppActivity.class);
                startActivity(intent);
                return true;

        }


        return super.onOptionsItemSelected(item);

    }


    public class DownloadImage extends AsyncTask<String, String, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... args) {
            InputStream inputStream;
            Bitmap bitmap;

            try {
                inputStream = (InputStream) new URL(args[0]).getContent();

                bitmap = BitmapFactory.decodeStream(inputStream);

                //in case the download fails
            }catch (IOException ex){
                bitmap = null;
            }

            return bitmap;
        }

        /*
         * Called after the image has been downloaded
         * -> this calls a function on the main thread again
         */
        protected void onPostExecute(Bitmap image){
            if(image != null){
                setImage(image);
            }
        }

    }
}
