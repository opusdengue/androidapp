package app.dengue.opus.les.br.denguereport.util.MapFilterAddress;

import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.dengue.opus.les.br.denguereport.DengueMainActivity;
import app.dengue.opus.les.br.denguereport.R;

/**
 * Created by leonardo on 12/7/15.
 */
public class SearchAutoCompleteAdapter implements SearchView.OnQueryTextListener {

    private static final int MAX_RESULTS = 1;
    private Activity activity;
    private SearchView searchView;
    private List<GeoSearchResult> resultList = new ArrayList();

    public SearchAutoCompleteAdapter(Activity activity, SearchView searchView) {
        this.activity = activity;
        this.searchView = searchView;
    }

    private List<GeoSearchResult> findLocations(String query_text) {

        List<GeoSearchResult> searchResults = new ArrayList<>();

        Geocoder geocoder = new Geocoder(activity, activity.getResources().getConfiguration().locale);
        List<Address> addresses = null;

        try {
            // Getting a maximum of MAX_RESULTS Address that matches the input text
            addresses = geocoder.getFromLocationName(query_text, MAX_RESULTS);

            for(int i = 0; i < addresses.size(); i++){
                Address address = addresses.get(i);
                if(address.getMaxAddressLineIndex() != -1)
                {
                    searchResults.add(new GeoSearchResult(address));
                }
            }


        } catch (IOException e) {
            return null;
        }

        return searchResults;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        resultList = findLocations(query);

        if(resultList != null){
            ((DengueMainActivity) activity).moveCameraToMap(resultList.get(0).getLocation());
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.setIconified(true);
            return true;
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        return false;
    }
}

