package app.dengue.opus.les.br.denguereport.shownotification.markerdetails;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.lucasr.twowayview.TwoWayView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import app.dengue.opus.les.br.denguereport.AboutAppActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldValueParcelable;
import app.dengue.opus.les.br.denguereport.shownotification.SaveAsyncTaskFragment;
import app.dengue.opus.les.br.denguereport.util.AlertDialogInterface;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;
import app.dengue.opus.les.br.denguereport.util.TweetParcelable;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.fields.FieldValue;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

/**
 * Created by leonardo on 10/12/15.
 */
@TargetApi(21)
public class ShowPoiDetails extends AppCompatActivity implements MarkerDetails{
    private ProgressBar progressBar;
    private TwoWayView imageView;
    private GridViewAdapter gridAdapter;
    private SaveAsyncTaskFragment saveAsyncTaskFragment;
    private TemporaryPoi temporaryPoi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_poi_details_layout);

        if(savedInstanceState == null){
            saveAsyncTaskFragment = new SaveAsyncTaskFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(saveAsyncTaskFragment, SaveAsyncTaskFragment.IDENTIFIER)
                    .commit();
        }else{
            saveAsyncTaskFragment = (SaveAsyncTaskFragment) getSupportFragmentManager()
                    .findFragmentByTag(SaveAsyncTaskFragment.IDENTIFIER);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        temporaryPoi = getIntent().getParcelableExtra(EXTRA);


        if(temporaryPoi != null){
            saveAsyncTaskFragment.sartTastToGetPoi(temporaryPoi.getId());
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    //populate the components with the tweet info
    public void addUIComponents(PointOfInterest poi){
        TextView tvTitle = (TextView) findViewById(R.id.textViewTitle);
        TextView tvDescription = (TextView) findViewById(R.id.textViewDescription);
        TextView tvTypeDate = (TextView) findViewById(R.id.textViewTypeDate);
        TextView tvDetails = (TextView) findViewById(R.id.textViewDetails);


        //in case of some pois do not have title or description
        if(poi.getTitle() != null){
            tvTitle.setText(poi.getTitle());
        }else{
            tvTitle.setText(R.string.form_title);
        }

        if(poi.getDescription() != null){
            tvDescription.setText(poi.getDescription());
        }else{
            tvDescription.setText("");
        }

        DateFormat dateFormat = DateFormat.getDateTimeInstance();

        tvTypeDate.setText(poi.getType().getName() + "\n");
        tvTypeDate.append(dateFormat.format(poi.getDate()));


        StringBuffer details = new StringBuffer();

        int i = 0;
        //brose the arraylist with the details about the poi
        for(FieldValue fvp: poi.getFieldValues()){
            details.append(++i + ". " + fvp.getField().getName() + "\n" + fvp.getValue() + "\n\n");
        }

        //There is no detail to show
        if(poi.getFieldValues().size() < 1){
            TextView textViewDetails = (TextView) findViewById(R.id.textViewDetailsLabel);
            textViewDetails.setVisibility(View.GONE);

            tvDetails.setVisibility(View.GONE);

        }else{
            tvDetails.setText(details);
        }

        CardView cardView = (CardView) findViewById(R.id.userInfo_card_view);
        cardView.setVisibility(View.VISIBLE);

        if(poi.getPictures().size() > 0) {
            saveAsyncTaskFragment.startTaskToGetPictures(poi.getPictures());
        }else{
            hideProgressBar();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dengue_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(this, AboutAppActivity.class);
                startActivity(intent);
                return true;

        }


        return super.onOptionsItemSelected(item);

    }

    public void showImages(List<Bitmap> bitmaps) {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();

        for(Bitmap bitmap: bitmaps){
            ImageItem item = new ImageItem(bitmap);
            imageItems.add(item);


        }

        imageView = (TwoWayView) findViewById(R.id.imageView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, imageItems);
        imageView.setAdapter(gridAdapter);

        imageView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //DownloadImage downloadImage = new DownloadImage();
                //downloadImage.execute(temporaryPoi.getPictures().get(position).getId());
            }
        });

    }

    public void hideProgressBar() {
        if(progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void loadPhoto(Bitmap bitmap) {

        //ImageView tempImageView = imageView;


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.full_image_dialog,
                (ViewGroup) findViewById(R.id.layout_root));

        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
        image.setImageBitmap(bitmap);
        //image.setImageDrawable(tempImageView.getDrawable());
        imageDialog.setView(layout);
        imageDialog.setPositiveButton("ok", new DialogInterface.OnClickListener(){

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });


        imageDialog.create();
        imageDialog.show();
    }

    public class DownloadImage extends AsyncTask<Long, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Long... args) {
            InputStream inputStream;
            Bitmap bitmap;

            SdkURIBuilder builder = new SdkURIBuilder();

            builder.setPathPoi("/picture/" + args[0].toString() + "/download");

            String url = builder.getUrlString();

            try {
                inputStream = (InputStream) new URL(url).getContent();

                bitmap = BitmapFactory.decodeStream(inputStream);

                //in case the download fails
            } catch (IOException ex) {
                bitmap = null;
            }

            return bitmap;
        }

        /*
         * Called after the image has been downloaded
         * -> this calls a function on the main thread again
         */
        protected void onPostExecute(Bitmap image) {
            if (image != null) {
                loadPhoto(image);
            }
        }
    }
}
