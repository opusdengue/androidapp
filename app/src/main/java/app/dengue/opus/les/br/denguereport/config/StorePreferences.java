package app.dengue.opus.les.br.denguereport.config;

import android.content.SharedPreferences;

import com.google.gson.Gson;


/**
 * Created by leonardo on 6/25/15.
 */
public class StorePreferences {
    private static String StringPreference = "StorePreferences.USER";

    public static void storePreferences(SharedPreferences preferences, UserPreferences user){
        String userJson = new Gson().toJson(user);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(StringPreference, userJson);

        editor.commit();

    }

    public static UserPreferences getUserPreferences(SharedPreferences preferences){
        String userJson = preferences.getString(StringPreference, new Gson().toJson(new UserPreferences()));

        return new Gson().fromJson(userJson, UserPreferences.class);
    }
}
