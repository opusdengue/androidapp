package app.dengue.opus.les.br.denguereport.shownotification.asynctasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Timer;

import app.dengue.opus.les.br.denguereport.DengueMainActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.shownotification.PlotMarker;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.InstagramEnvelope;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;

/**
 * Created by leonardo on 12/8/15.
 */
public class AsyncTasktoGetInstagram extends AsyncTask<Void, Void, InstagramEnvelope> {
    private Activity activity;
    private PlotMarker plotMarker;
    private Timer timer;

    public AsyncTasktoGetInstagram(Activity activity, Timer timer) {
        this.activity = activity;
        this.timer = timer;
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }

    @Override
    protected InstagramEnvelope doInBackground(Void... params) {
        PoiConsumer consumer = new PoiConsumer();

        try {
            return consumer.getAllInstagramPosts();
        }catch (SdkDengueException e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(InstagramEnvelope instagramEnvelope) {
        if(instagramEnvelope == null){
            timer.cancel();
            if(activity != null) {
                ((DengueMainActivity) activity).displayErrorInfo();
            }
            return;
        }

        if(activity != null){
            timer.cancel();

            plotMarker = new PlotMarker(
                    ((DengueMainActivity)activity).getMap(),
                    activity
            );
            plotMarker.plotInstagramPosts(instagramEnvelope);

            String sToast = activity.getResources().getString(R.string.toast_show_number_notifications);

            sToast = String.format(sToast, instagramEnvelope.getContent().size());

            Toast.makeText(activity, sToast, Toast.LENGTH_SHORT).show();
        }
    }
}
