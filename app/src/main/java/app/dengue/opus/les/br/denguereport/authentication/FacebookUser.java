package app.dengue.opus.les.br.denguereport.authentication;

import android.os.Parcel;
import android.os.Parcelable;

import com.facebook.AccessToken;
import com.facebook.Profile;

/**
 * Created by leonardo on 11/11/15.
 */
public class FacebookUser extends AuthenticationUser implements Parcelable{
    private AccessToken accessToken;
    private Profile profile;
    private String urlCover;
    //controls the login section -> true is connected and false is disconnected
    private boolean connected;

    public FacebookUser() {
        super();
        urlCover = new String();
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profileTracker) {
        this.profile = profileTracker;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(accessToken, flags);
        dest.writeParcelable(profile, flags);
        if(connected){
            dest.writeByte((byte) 1);
        }else{
            dest.writeByte((byte) 0);
        }

        dest.writeString(urlCover);

    }

    public static final Parcelable.Creator<FacebookUser> CREATOR
            = new Parcelable.Creator<FacebookUser>(){

        @Override
        public FacebookUser createFromParcel(Parcel source) {
            return new FacebookUser(source);
        }

        @Override
        public FacebookUser[] newArray(int size) {
            return new FacebookUser[size];
        }
    };

    private FacebookUser(Parcel in) {
        setAccessToken(in.<AccessToken>readParcelable(AccessToken.class.getClassLoader()));
        setProfile(in.<Profile>readParcelable(Profile.class.getClassLoader()));
        setConnected((in.readByte() != 0));
        setUrlCover(in.readString());
    }


    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public void setUrlCover(String urlCover){
        this.urlCover = urlCover;
    }

    public String getUrlCover(){
        return urlCover;
    }

    @Override
    public String toString() {
        return "FacebookUser{" +
                "profile=" + profile.getName() +
                '}';
    }
}
