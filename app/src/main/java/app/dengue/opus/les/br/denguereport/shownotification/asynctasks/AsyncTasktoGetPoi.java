package app.dengue.opus.les.br.denguereport.shownotification.asynctasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PictureParcelable;
import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.ShowPoiDetails;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

/**
 * Created by leonardo on 7/2/15.
 */
public class AsyncTasktoGetPoi extends AsyncTask<Long, Void, PointOfInterest> {
    private Activity activity;

    public AsyncTasktoGetPoi(Activity activity){
        onAttach(activity);
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }


    @Override
    protected PointOfInterest doInBackground(Long... params) {
        PoiConsumer consumer = new PoiConsumer();

        try {
            return consumer.getPointofInterest(params[0]);
        }catch (SdkDengueException e){
            return null;
        }
    }


    @Override
    protected void onPostExecute(PointOfInterest poi) {
        if(poi == null){
            return;
        }

        if(activity != null){

            ((ShowPoiDetails) activity).addUIComponents(poi);
        }

    }

}
