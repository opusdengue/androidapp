package app.dengue.opus.les.br.denguereport.shownotification.markerdetails;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.dengue.opus.les.br.denguereport.R;

public class GridViewAdapter extends ArrayAdapter<ImageItem> {
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<ImageItem> mGridData = new ArrayList<ImageItem>();

    public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<ImageItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) row.findViewById(R.id.grid_item_image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ImageItem item = mGridData.get(position);
        holder.imageView.setImageBitmap(item.getImage());

        return row;
    }

    static class ViewHolder {
        ImageView imageView;
    }
}