package app.dengue.opus.les.br.denguereport.util;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PictureParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PoiTypeParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldValueParcelable;
import br.les.opus.dengue.sdk.domain.LatLng;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.domain.PoiType;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.fields.FieldValue;

/**
 * Created by leonardo on 6/17/15.
 */
public class TemporaryPoi implements Parcelable{
    private Long id;
    private String title;
    private String description;
    private Date date = new Date();
    private PoiTypeParcelable poiTypeParcelable;
    private List<FieldValueParcelable> fieldValueParcelables;
    //private long[] pictureIds;
    private List<PictureParcelable> pictures = new ArrayList<>();



    public TemporaryPoi(String title, String description, PoiTypeParcelable poiTypeParcelable) {
        this.id = 0L;
        this.title = title;
        this.description = description;

        this.poiTypeParcelable = poiTypeParcelable;
        //pictureIds = new long[10];
    }

    public TemporaryPoi(PointOfInterest pointOfInterest){
        if(pointOfInterest.getId() == null){
            this.setId(0L);
        }else {
            this.setId(pointOfInterest.getId());
        }

        this.setTitle(pointOfInterest.getTitle());
        this.setDescription(pointOfInterest.getDescription());
        this.setDate(pointOfInterest.getDate());
        fieldValueParcelables = new ArrayList<>();



        List<Picture> picturesPoi = pointOfInterest.getPictures();

        for (Picture pic: picturesPoi){
            pictures.add(new PictureParcelable(pic.getId()));
        }

        poiTypeParcelable = new PoiTypeParcelable(pointOfInterest.getType());

        if(pointOfInterest.getFieldValues() != null) {

            for (FieldValue fv : pointOfInterest.getFieldValues()) {
                fieldValueParcelables.add(new FieldValueParcelable(fv.getValue(), fv.getField().getId(), fv.getField().getName()));
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<FieldValueParcelable> getFieldValueParcelables() {
        return fieldValueParcelables;
    }

    public void setFieldValueParcelables(List<FieldValueParcelable> fieldValueParcelables) {
        this.fieldValueParcelables = fieldValueParcelables;
    }

    public List<PictureParcelable> getPictures() {
        return pictures;
    }

    public void setPictures(List<PictureParcelable> pictures) {
        this.pictures = pictures;
    }

    public PoiType getPoiType() {
        PoiType poiType = new PoiType();
        poiType.setId(poiTypeParcelable.getId());
        poiType.setName(poiTypeParcelable.getName());
        poiType.setFields(poiTypeParcelable.getFields());

        return poiType;
    }


    public PoiTypeParcelable getPoiTypeParcelable() {
        return poiTypeParcelable;
    }

    public void setPoiTypeParcelable(PoiTypeParcelable poiTypeParcelable) {
        this.poiTypeParcelable = poiTypeParcelable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(poiTypeParcelable, flags);

        dest.writeString(getTitle());
        dest.writeString(getDescription());

        dest.writeLong(getId());

        dest.writeLong(date.getTime());

        dest.writeTypedList(fieldValueParcelables);

        dest.writeTypedList(pictures);

        //dest.writeLongArray(pictureIds);

    }

    public static final Parcelable.Creator<TemporaryPoi> CREATOR
            = new Parcelable.Creator<TemporaryPoi>(){

        @Override
        public TemporaryPoi createFromParcel(Parcel source) {
            return new TemporaryPoi(source);
        }

        @Override
        public TemporaryPoi[] newArray(int size) {
            return new TemporaryPoi[size];
        }
    };

    private TemporaryPoi(Parcel in) {
        setPoiTypeParcelable((PoiTypeParcelable) in.readParcelable(PoiTypeParcelable.class.getClassLoader()));
        setTitle(in.readString());
        setDescription(in.readString());
        setId(in.readLong());
        setDate(new Date(in.readLong()));

        fieldValueParcelables = new ArrayList<>();
        in.readTypedList(fieldValueParcelables, FieldValueParcelable.CREATOR);

        pictures = new ArrayList<>();
        in.readTypedList(pictures, PictureParcelable.CREATOR);

    }

    public static PointOfInterest convertToPointOfInterest(TemporaryPoi temporaryPoi, Picture picture, Location location){
        PointOfInterest poi = new PointOfInterest();

        poi.setTitle(temporaryPoi.getTitle());
        poi.setDescription(temporaryPoi.getDescription());
        poi.setLocation(new LatLng(location.getLatitude(), location.getLongitude()));

        PoiType type = temporaryPoi.getPoiType();
        poi.setType(type);

        List<FieldValue> fieldValues = new ArrayList<>();

        for(FieldValueParcelable fdp: temporaryPoi.getFieldValueParcelables()){
            fieldValues.add(fdp.convertToFieldValue());
        }

        poi.setFieldValues(fieldValues);

        List<Picture> documentos = new ArrayList<>();

        if( picture != null) {
            documentos.add(picture);
        }

        poi.setPictures(documentos);

        return poi;
    }

    @Override
    public String toString() {
        return "TemporaryPoi{" +
                "Id='" + id + '\n' +
                "title='" + title + '\n' +
                ", description='" + description + '\n' +
                ", values" + fieldValueParcelables +
                '}';
    }
}
