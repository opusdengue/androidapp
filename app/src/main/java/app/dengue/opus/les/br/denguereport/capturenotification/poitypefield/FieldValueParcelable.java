package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;


import android.os.Parcel;
import android.os.Parcelable;

import br.les.opus.dengue.sdk.domain.fields.Field;
import br.les.opus.dengue.sdk.domain.fields.FieldValue;

/**
 * Created by leonardo on 7/22/15.
 */
public class FieldValueParcelable implements Parcelable{
    private Long idField;
    private String value;
    private String label;

    public FieldValueParcelable(String value, Long idField){
        this.value = value;
        this.idField = idField;
        label = "";
    }

    public FieldValueParcelable(String value, Long idField, String label){
        this.value = value;
        this.idField = idField;
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(idField);
        dest.writeString(value);
        dest.writeString(label);
    }

    public static final Parcelable.Creator<FieldValueParcelable> CREATOR
            = new Parcelable.Creator<FieldValueParcelable>(){
        @Override
        public FieldValueParcelable createFromParcel(Parcel source) {
            return new FieldValueParcelable(source);
        }

        @Override
        public FieldValueParcelable[] newArray(int size) {
            return new FieldValueParcelable[size];
        }
    };

    private FieldValueParcelable(Parcel in){
        idField = in.readLong();
        value = in.readString();
        label = in.readString();
    }

    public FieldValue convertToFieldValue(){
        FieldValue fieldValue = new FieldValue();
        fieldValue.setValue(value);
        fieldValue.setField(new Field(idField, label));

        return fieldValue;
    }

    @Override
    public String toString() {
        return "FieldValueParcelable{" +
                "idField= " + idField +
                ", label= " + label +
                ", value= " + value + "}" ;
    }
}
