package app.dengue.opus.les.br.denguereport.shownotification.markerdetails;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;

import app.dengue.opus.les.br.denguereport.AboutAppActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.util.InstagramParcelable;
import app.dengue.opus.les.br.denguereport.util.TweetParcelable;
import br.les.opus.dengue.sdk.domain.PointOfInterest;

/**
 * Created by leonardo on 10/12/15.
 */
public class ShowInstagramPostDetails extends AppCompatActivity implements MarkerDetails{
    private InstagramParcelable instagramParcelable;
    private ImageView imageViewProfifle;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_instagram_details_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        instagramParcelable = getIntent().getParcelableExtra(EXTRA);

        imageViewProfifle = (ImageView) findViewById(R.id.imageViewUser);
        imageView = (ImageView) findViewById(R.id.instagramImageView);

        if(instagramParcelable != null){
            addUIComponents();
        }


    }


    @Override
    public void addUIComponents(PointOfInterest poi){

    }

    //populate the components with the post info
    public void addUIComponents(){
        TextView tvUserName = (TextView) findViewById(R.id.textViewUserName);
        TextView tvProfile = (TextView) findViewById(R.id.textViewUser);
        TextView tvTweetDescription = (TextView) findViewById(R.id.textViewDescription);
        TextView tvTweetDate = (TextView) findViewById(R.id.textViewDate);

        tvUserName.setText(instagramParcelable.getUser().getUser());
        tvProfile.setText(instagramParcelable.getUser().getProfile());
        tvTweetDescription.setText(instagramParcelable.getTextComment());


        DateFormat dateFormat = DateFormat.getDateTimeInstance();

        tvTweetDate.setText(dateFormat.format(instagramParcelable.getCreatedAt()));

        String urlProfilePicture = instagramParcelable.getUser().getUrlProfile();


        /*
        * try to download the post picture.
        */
        new DownloadImage().execute(instagramParcelable.getUrlPicture());

        /*
        * try to download the uses image. If the download fail, the default image is put in its place
        */
        new DownloadImageProfile().execute(urlProfilePicture);

    }


    private void setImageProfile(Bitmap image){
        imageViewProfifle.setImageBitmap(image);
    }

    private void setImage(Bitmap image){
        imageView.setImageBitmap(image);
    }


    //when the user clicks in the instagram button
    public void goToInstagram(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(instagramParcelable.getUrlInstagram()));
        startActivity(browserIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dengue_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(this, AboutAppActivity.class);
                startActivity(intent);
                return true;

        }


        return super.onOptionsItemSelected(item);

    }

    //AsyncTask to download the instagram picture
    public class DownloadImage extends AsyncTask<String, String, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... args) {
            InputStream inputStream;
            Bitmap bitmap;

            try {
                inputStream = (InputStream) new URL(args[0]).getContent();

                bitmap = BitmapFactory.decodeStream(inputStream);

                //in case the download fails
            }catch (IOException ex){
                bitmap = null;
            }

            return bitmap;
        }

        /*
         * Called after the image has been downloaded
         * -> this calls a function on the main thread again
         */
        protected void onPostExecute(Bitmap image){
            if(image != null){
                setImage(image);
            }
        }

    }

    //AsyncTask to download the user's picture
    public class DownloadImageProfile extends AsyncTask<String, String, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... args) {
            InputStream inputStream;
            Bitmap bitmap;

            try {
                inputStream = (InputStream) new URL(args[0]).getContent();

                bitmap = BitmapFactory.decodeStream(inputStream);

                //in case the download fails
            }catch (IOException ex){
                bitmap = null;
            }

            return bitmap;
        }

        /*
         * Called after the image has been downloaded
         * -> this calls a function on the main thread again
         */
        protected void onPostExecute(Bitmap image){
            if(image != null){
                setImageProfile(image);
            }
        }

    }
}
