package app.dengue.opus.les.br.denguereport.serverHandler;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldValueParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PictureParcelable;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.endpoints.PoiProducer;
import br.les.opus.dengue.sdk.endpoints.UploadPicture;

/**
 * Created by leonardo on 6/13/15.
 *
 * Class responsible for sending the poi and picture to the server
 *
 */
public class SendPoiIntentService extends IntentService{
    public final static String RESPONSE_STRING = "SendPoiService.POITYPERECEIVE";
    public static final String LOCATION = "SendPoiService.LOCATION";
    public static String POI = "SendPoiService.POINTOFINTEREST";
    public static String PICTURE = "SendPoiService.PICTURE";
    public final static String STATUS = "PoiTypeIntentService.CONNECTED";

    public SendPoiIntentService(){
        super("SendPoiService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SendPoiIntentService.RESPONSE_STRING);

        Location location = intent.getParcelableExtra(SendPoiIntentService.LOCATION);
        TemporaryPoi temporaryPoi = intent.getParcelableExtra(SendPoiIntentService.POI);

        File file = null;

        try{
            Picture picture = null;

            if(temporaryPoi.getPictures().size() > 0) {
                System.out.println(temporaryPoi.getPictures().get(0).getLocalUrl());
                file = new File(temporaryPoi.getPictures().get(0).getLocalUrl());
                picture = sendPicture(file);

                List<PictureParcelable> pictureParcelableList = new ArrayList<>();
                pictureParcelableList.add(new PictureParcelable(picture.getId()));
                temporaryPoi.setPictures(pictureParcelableList);
            }

            PointOfInterest pointOfInterest = sendPoiAnonymous(temporaryPoi, picture, location);

            temporaryPoi.setDate(pointOfInterest.getDate());
            temporaryPoi.setId(pointOfInterest.getId());

            //ArrayList<FieldValueParcelable> fieldValueParcelableArrayList = new ArrayList<>();
            broadcastIntent.putExtra(SendPoiIntentService.POI, temporaryPoi);

            broadcastIntent.putExtra(SendPoiIntentService.LOCATION, location);

            broadcastIntent.putExtra(GetPoiTypeIntentService.STATUS, true);
        }catch (SdkDengueException e){
            Logger.getLogger("").log(Level.SEVERE, "Erro", e);
            broadcastIntent.putExtra(GetPoiTypeIntentService.STATUS, false);
        }

        sendBroadcast(broadcastIntent);

    }

    private Picture sendPicture(File file) throws SdkDengueException{
        Picture picture;

        UploadPicture uploadPicture = new UploadPicture();

        picture = uploadPicture.upload(file);

        return picture;

    }

    //send the poi to server using the Dengue SDK by anonymous way
    private PointOfInterest sendPoiAnonymous(TemporaryPoi temporaryPoi, Picture picture, Location location) throws SdkDengueException{
        PointOfInterest poi = TemporaryPoi.convertToPointOfInterest(temporaryPoi, picture, location);

        poi.setAddress(getLocation(location));

        //send data as null, the server handles with its own data
        poi.setDate(null);

        //send the poi to server
        PoiProducer producer = new PoiProducer();

        return producer.sendPoiAnonymous(poi);

    }

    private String getLocation(Location location){
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        StringBuffer addressBuffer = new StringBuffer();

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems
            return "";

        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            return "";
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size()  == 0) {
            return "";
        } else {
            Address address = addresses.get(0);

            try {
                addressBuffer.append(address.getThoroughfare());
                addressBuffer.append(", " + address.getLocality());
                addressBuffer.append(", " + address.getAdminArea());
                addressBuffer.append(", " + address.getCountryName());

            }catch (NullPointerException e){
                addressBuffer.append("");
            }

            return addressBuffer.toString();
        }

    }
}
