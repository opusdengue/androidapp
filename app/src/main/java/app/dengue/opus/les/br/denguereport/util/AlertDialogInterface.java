package app.dengue.opus.les.br.denguereport.util;

/**
 * Created by leonardo on 6/12/15.
 */
public interface AlertDialogInterface {
    public abstract void onButtonClicked(boolean value);
}
