package app.dengue.opus.les.br.denguereport.capturenotification.camera;

import android.os.Environment;

import java.io.File;

/**
 * Created by leonardo on 5/18/15.
 */
public class BaseAlbumDirFactory {
    // Standard storage location for digital camera files
    private static final String CAMERA_DIR = "/dcim/";


    public File getAlbumStorageDir(String albumName) {
        return new File(
                Environment.getExternalStorageDirectory()
                        + CAMERA_DIR
                        + albumName
        );
    }
}
