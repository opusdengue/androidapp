package app.dengue.opus.les.br.denguereport.capturenotification;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.dengue.opus.les.br.denguereport.animation.FloatingHintEditText;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldOptionParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldParcelable;

/**
 * Created by leonardo on 12/9/15.
 */
public class ContainerFieldView {
    private FieldParcelable fieldParcelable;
    private Spinner spinner;
    private HashMap<String, String> spinnerHash;   //value selected through spinner
    private FloatingHintEditText editText;          //age

    public ContainerFieldView(Spinner spinner, FieldParcelable fieldParcelable) {
        this.spinner = spinner;
        this.fieldParcelable = fieldParcelable;
    }

    public ContainerFieldView(FloatingHintEditText editText, FieldParcelable fieldParcelable) {
        this.editText = editText;
        this.fieldParcelable = fieldParcelable;
    }

    public FieldParcelable getFieldParcelable() {
        return fieldParcelable;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void populateSpinnerTypeNotifications(Activity activity, FieldParcelable fieldParcelable){
        List<FieldOptionParcelable> fieldOptions =  fieldParcelable.getFieldOptionsParcelable();

        ArrayList<String> spinnerValues = new ArrayList<>();
        spinnerHash = new HashMap<>();

        for(FieldOptionParcelable option: fieldOptions){
            spinnerValues.add(option.getLabel());
            spinnerHash.put(option.getLabel(), option.getValue());
        }

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(
                activity, android.R.layout.simple_spinner_item, spinnerValues);

        typeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner.setAdapter(typeAdapter);
    }

    public String getEditText() {
        return editText.getText().toString();
    }

    public String getSpinnerValue(String key){
        return spinnerHash.get(key);
    }


}
