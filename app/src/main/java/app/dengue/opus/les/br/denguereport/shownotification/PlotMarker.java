package app.dengue.opus.les.br.denguereport.shownotification;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;

import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.MarkerDetails;
import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.ShowInstagramPostDetails;
import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.ShowPoiDetails;
import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.ShowTweetDetails;
import app.dengue.opus.les.br.denguereport.util.InstagramParcelable;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;
import app.dengue.opus.les.br.denguereport.util.TweetParcelable;
import br.les.opus.dengue.sdk.domain.InstagramEnvelope;
import br.les.opus.dengue.sdk.domain.PoiEnvelope;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.Tweet;
import br.les.opus.dengue.sdk.domain.TweetEnvelope;
import br.les.opus.dengue.sdk.domain.instagram.Instagram;
import br.les.opus.dengue.sdk.domain.instagram.Location;

/**
 * Created by leonardo on 6/16/15.
 */
public class PlotMarker implements GoogleMap.OnInfoWindowClickListener{
    private GoogleMap map;
    private MarkerOptions markerOptions;
    private Activity activity;
    private HashMap<Marker, Tweet> tweetHashMap;
    private HashMap<Marker, PointOfInterest> poiHashMap;
    private HashMap<Marker, Instagram> instagramHashMap;

    public PlotMarker(GoogleMap map, Activity activity){
        this.activity = activity;
        this.map = map;
        this.map.setOnInfoWindowClickListener(this);

        tweetHashMap = new HashMap<>();
        poiHashMap = new HashMap<>();
        instagramHashMap = new HashMap<>();
    }

    public LatLng plotPointOfInterest(PointOfInterest poi){
        String sTitle = poi.getTitle().toUpperCase();
        String sDesc = poi.getDescription();

        LatLng position;

        markerOptions = new MarkerOptions();

        StringBuffer description = new StringBuffer();

        if(sDesc != null && !sDesc.isEmpty()){
            description.append(sDesc + "\n");
        }


        if(sTitle != null && !sTitle.isEmpty()){
            markerOptions.title(sTitle);
        }else{
            markerOptions.title(sTitle);
        }

        DateFormat dateFormat = DateFormat.getDateTimeInstance();

        description.append(dateFormat.format(poi.getDate()));

        markerOptions.snippet(description.toString());

        position = new LatLng(poi.getLocation().getLat(), poi.getLocation().getLng());
        markerOptions.position(position);

        //define the color of the icon based on type of the poi
        defineIconColor(poi.getType().getId());

        Marker m = map.addMarker(markerOptions);


        poiHashMap.put(m, poi);


        return position;

    }

    public void plotPointOfInterests(PoiEnvelope envelope){
        for(PointOfInterest poi: envelope.getContent()){
            plotPointOfInterest(poi);
        }

        map.animateCamera(CameraUpdateFactory.zoomTo(map.getMinZoomLevel()));
    }

    private void defineIconColor(long type){
        if (type == 1 || type == 2 || type == 4 || type == 5){
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }else{
            if (type == 3){
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            }else{
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }
        }
    }

    private void defineTweetIconColor(String label){
        if (label.equals("SICKNESS")){
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }else{
            if (label.equals("MOSQUITO_FOCUS")){
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            }else{
                if(label.equals("JOKE")){
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                }else{
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                }
            }
        }
    }

    public void plotTweets(TweetEnvelope tweetEnvelope) {
        for(Tweet tweet: tweetEnvelope.getContent()){
            //post only tweets that are not about jokes
            if(tweet.getClassification() != null) {
                plotTweet(tweet);
            }
        }

        map.animateCamera(CameraUpdateFactory.zoomTo(map.getMinZoomLevel()));
    }

    public void plotTweet(Tweet tweet){
        String user = tweet.getUser().getName();
        StringBuffer description = new StringBuffer();
        LatLng position;

        description.append(tweet.getText());

        markerOptions = new MarkerOptions();

        markerOptions.title(user);

        if(tweet.getClassification() != null){
            markerOptions.snippet(description.toString());
            //        activity.getString(R.string.tweet_classification) + " " + tweet.getClassification().getLabel());

            //define the color of the icon based on type of the poi
            defineTweetIconColor(tweet.getClassification().getKey());
        }else{
            markerOptions.snippet(description.toString());
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        }

        position = new LatLng(tweet.getGeolocation().getLat(), tweet.getGeolocation().getLng());
        markerOptions.position(position);

        tweetHashMap.put(map.addMarker(markerOptions), tweet);

    }

    public void plotInstagramPosts(InstagramEnvelope instagramEnvelope) {
        for(Instagram insta: instagramEnvelope.getContent()){
            plotInstagram(insta);
        }

        map.animateCamera(CameraUpdateFactory.zoomTo(map.getMinZoomLevel()));
    }

    public void plotInstagram(Instagram instagram){
        String user = instagram.getUser().getFullName();
        String description = instagram.getCaption().getText();
        Location location = instagram.getLocation();
        LatLng position;

        markerOptions = new MarkerOptions();

        markerOptions.title(user);
        if(description != null) {
            markerOptions.snippet(description);
        }

        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        position = new LatLng(location.getLatitude(), location.getLongitude());
        markerOptions.position(position);

        instagramHashMap.put(map.addMarker(markerOptions), instagram);

    }


    public void plotHitMap(List<LatLng> pois) {
        HeatmapTileProvider heatmapTileProvider = new HeatmapTileProvider.Builder()
                .data(pois)
                .build();

        heatmapTileProvider.setGradient(HeatmapTileProvider.DEFAULT_GRADIENT);
        heatmapTileProvider.setOpacity(HeatmapTileProvider.DEFAULT_OPACITY);

        map.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));

        map.animateCamera(CameraUpdateFactory.zoomTo(map.getMinZoomLevel()));
    }

    //method is called when the user clicks on the notification
    @Override
    public void onInfoWindowClick(Marker marker) {
        Tweet tweet = tweetHashMap.get(marker);

        //check if the click was on a tweet or a regular poi
        if(tweet != null) {
            startActivityShowDetails(ShowTweetDetails.class, new TweetParcelable(tweet));
        }else {
            Instagram insta = instagramHashMap.get(marker);

            if(insta != null){
                startActivityShowDetails(ShowInstagramPostDetails.class, new InstagramParcelable(insta));
            }else {
                PointOfInterest poi = poiHashMap.get(marker);

                TemporaryPoi temporaryPoi = new TemporaryPoi(poi);
                temporaryPoi.setId(poi.getId());

                startActivityShowDetails(ShowPoiDetails.class, temporaryPoi);
            }
        }

    }

    private void startActivityShowDetails(Class markerDetails, Parcelable parcelable){
        Intent intent = new Intent(activity, markerDetails);
        intent.putExtra(MarkerDetails.EXTRA, parcelable);

        activity.startActivity(intent);
    }


}
