package app.dengue.opus.les.br.denguereport.config;

/**
 * Created by leonardo on 6/25/15.
 */
public class UserPreferences {
    private boolean isConnected;
    private String extra;

    public UserPreferences(){
        this.isConnected = false;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setExtra(String extra){
        this.extra = extra;
    }

    public String getExtra(){
        return extra;
    }

    public void setConnected(boolean connected) {
        this.isConnected = connected;
    }

}
