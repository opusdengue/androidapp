package app.dengue.opus.les.br.denguereport.shownotification;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PictureParcelable;
import app.dengue.opus.les.br.denguereport.shownotification.asynctasks.AsyncTasktoGetHeatmap;
import app.dengue.opus.les.br.denguereport.shownotification.asynctasks.AsyncTasktoGetInstagram;
import app.dengue.opus.les.br.denguereport.shownotification.asynctasks.AsyncTasktoGetPictures;
import app.dengue.opus.les.br.denguereport.shownotification.asynctasks.AsyncTasktoGetPoi;
import app.dengue.opus.les.br.denguereport.shownotification.asynctasks.AsyncTasktoGetPois;
import app.dengue.opus.les.br.denguereport.shownotification.asynctasks.AsyncTasktoGetTweets;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.Picture;

/**
 * Created by leonardo on 7/2/15.
 */
public class SaveAsyncTaskFragment extends Fragment{
    public static String IDENTIFIER = "SaveAsyncTaskInstance.TASKFRAGMENT";
    private AsyncTasktoGetPois asyncTaskPois;
    private AsyncTasktoGetPoi asyncTaskPoi;
    private AsyncTasktoGetTweets asyncTaskTweets;
    private AsyncTasktoGetInstagram asyncTasktoGetInstagram;
    private AsyncTasktoGetHeatmap asynctaskHeatmap;
    private AsyncTasktoGetPictures asyncTasktoGetPictures;
    private Timer timer;
    private Activity activity;
    private boolean activated;

    public SaveAsyncTaskFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //this will stop the destruction of the activity
        setRetainInstance(true);
    }

    public void startTaskToGetPoi(){
        createTimeToast();
        asyncTaskPois = new AsyncTasktoGetPois(activity, timer);
        asyncTaskPois.execute();
    }

    public void startTaskToGetTweets(){
        createTimeToast();
        asyncTaskTweets = new AsyncTasktoGetTweets(activity, timer);
        asyncTaskTweets.execute();

    }

    public void startTaskToGetHeatMap(){
        createTimeToast();
        asynctaskHeatmap = new AsyncTasktoGetHeatmap(activity, timer);
        asynctaskHeatmap.execute();
    }

    public void startTaskToGetInstagram(){
        createTimeToast();
        asyncTasktoGetInstagram = new AsyncTasktoGetInstagram(activity, timer);
        asyncTasktoGetInstagram.execute();
    }

    public void startTaskToGetPictures(List<Picture> pictures) {
        asyncTasktoGetPictures = new AsyncTasktoGetPictures(activity);
        asyncTasktoGetPictures.execute(pictures);
    }

    public void sartTastToGetPoi(Long id) {
        asyncTaskPoi = new AsyncTasktoGetPoi(activity);
        asyncTaskPoi.execute(id);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.activity = activity;

        if(asyncTaskPois != null){
            asyncTaskPois.onAttach(activity);
        }

        if(asyncTaskTweets != null){
            asyncTaskTweets.onAttach(activity);
        }

        if(asynctaskHeatmap != null){
            asynctaskHeatmap.onAttach(activity);
        }

        if (asyncTasktoGetPictures != null){
            asyncTasktoGetPictures.onAttach(activity);
        }

        if (asyncTaskPoi != null){
            asyncTaskPoi.onAttach(activity);
        }

        if (asyncTasktoGetInstagram != null){
            asyncTasktoGetInstagram.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (asyncTaskPois != null){
            asyncTaskPois.onDetach();
        }

        if(asyncTaskTweets != null){
            asyncTaskTweets.onDetach();
        }

        if(asynctaskHeatmap != null){
            asynctaskHeatmap.onDetach();
        }

        if(asyncTasktoGetPictures != null){
            asyncTasktoGetPictures.onDetach();
        }

        if (asyncTaskPoi != null){
            asyncTaskPoi.onDetach();
        }

        if (asyncTasktoGetInstagram != null){
            asyncTasktoGetInstagram.onDetach();
        }
    }

    public boolean getActivated(){
        return activated;
    }

    public void setActivated(boolean activated){
        this.activated = activated;
    }

    private void createTimeToast(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            private Handler handler = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);
                    if(isAdded() && activity != null) {
                        Toast.makeText(activity.getApplicationContext(), getString(R.string.toast_searching), Toast.LENGTH_SHORT).show();
                    }
                }

            };

            @Override
            public void run() {
                try {
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 500, 5000);
    }

    public void cancelToast() {
        if (timer != null) {
            timer.cancel();
        }
    }

}
