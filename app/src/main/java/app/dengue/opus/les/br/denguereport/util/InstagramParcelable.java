package app.dengue.opus.les.br.denguereport.util;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import br.les.opus.dengue.sdk.domain.TwitterUser;
import br.les.opus.dengue.sdk.domain.instagram.Instagram;
import br.les.opus.dengue.sdk.domain.instagram.InstagramUser;


/**
 * Created by leonardo on 10/10/15.
 *
 * Parcelable class that will store a instagram post
 */
public class InstagramParcelable implements Parcelable{
    private String textComment;
    private Date createdAt;
    private String type;
    private String urlPicture;
    private String urlVideo;
    private String linkToInstagram;
    private InstagramUser user;

    public InstagramParcelable(Instagram instagram) {
        this.textComment = instagram.getCaption().getText();
        this.createdAt = instagram.getCreatedTime();
        this.user = new InstagramUser(instagram.getUser());
        this.type = instagram.getType();
        this.linkToInstagram = instagram.getLink();

        if(type.equals("image")) {
            this.urlPicture = instagram.getImages().getLowResolution().getUrl();
        }else{
            this.urlVideo = instagram.getVideos().getLowResolution().getUrl();
        }

    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrlPicture() {
        return urlPicture;
    }

    public void setUrlPicture(String urlPicture) {
        this.urlPicture = urlPicture;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getLinkToInstagram() {
        return linkToInstagram;
    }

    public void setLinkToInstagram(String linkToInstagram) {
        this.linkToInstagram = linkToInstagram;
    }

    public InstagramUser getUser() {
        return user;
    }

    public void setUser(InstagramUser user) {
        this.user = user;
    }

    //everything bellow is related to parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(textComment);
        dest.writeLong(createdAt.getTime());
        dest.writeString(type);
        dest.writeString(urlPicture);
        dest.writeString(urlVideo);
        dest.writeString(linkToInstagram);

        dest.writeParcelable(user, flags);
    }

    public static final Creator<InstagramParcelable> CREATOR
            = new Creator<InstagramParcelable>(){
        @Override
        public InstagramParcelable createFromParcel(Parcel source) {
            return new InstagramParcelable(source);
        }

        @Override
        public InstagramParcelable[] newArray(int size) {
            return new InstagramParcelable[size];
        }
    };

    private InstagramParcelable(Parcel in){
        this.textComment = in.readString();
        this.createdAt = new Date(in.readLong());
        this.type = in.readString();
        this.urlPicture = in.readString();
        this.urlVideo = in.readString();
        this.linkToInstagram = in.readString();
        this.user = in.readParcelable(InstagramUser.class.getClassLoader());
    }

    public String getUrlInstagram() {
        return linkToInstagram;
    }

    //Inner class to store the instagram User
    public static class InstagramUser implements Parcelable{
        private String user;
        private String profile;
        private String urlProfile;

        public InstagramUser(br.les.opus.dengue.sdk.domain.instagram.InstagramUser instagramUser) {
            this.user = instagramUser.getFullName();
            this.profile = instagramUser.getUsername();
            this.urlProfile = instagramUser.getProfilePictureUrl();
        }

        public String getUser() {
            return user;
        }

        public String getProfile() {
            return profile;
        }

        public String getUrlProfile(){
            return urlProfile;
        }

        //everything bellow is related to the parcelable
        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(user);
            dest.writeString(profile);
            dest.writeString(urlProfile);
        }

        public static final Creator<InstagramUser> CREATOR
                = new Creator<InstagramUser>(){
            @Override
            public InstagramUser createFromParcel(Parcel source) {
                return new InstagramUser(source);
            }

            @Override
            public InstagramUser[] newArray(int size) {
                return new InstagramUser[size];
            }
        };

        private InstagramUser (Parcel in){
            user = in.readString();
            profile = in.readString();
            urlProfile = in.readString();
        }
    }
}
