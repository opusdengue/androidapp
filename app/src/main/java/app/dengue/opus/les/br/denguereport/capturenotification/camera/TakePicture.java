package app.dengue.opus.les.br.denguereport.capturenotification.camera;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.capturenotification.CaptureNotificationActivity;
import app.dengue.opus.les.br.denguereport.serverHandler.SendPoiIntentService;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;

/**
 * Created by leonardo on 5/18/15.
 */
public class TakePicture extends Activity {
    public static final String TAG = "TAKEPICTUREACTIVITY";
    public static final String NOPICTURE = "CaptureNotificationActivity.NOPICTURE";
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_CAPTURE = 2;
    private String mCurrentPhotoPath;

    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    private BaseAlbumDirFactory mAlbumStorageDirFactory = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mAlbumStorageDirFactory = new BaseAlbumDirFactory();

        dispatchTakePictureIntent();
    }

    private void dispatchTakePictureIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file;

        try{
            file = setUpPhotoFile();
            mCurrentPhotoPath = file.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        }catch (IOException e){
            Log.e(TAG, "Erro ao invocar aplicativo de câmera");
            mCurrentPhotoPath = null;
        }

        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();

        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);

        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (! storageDir.mkdirs()) {
                    if (! storageDir.exists()){
                        Log.d(getString(R.string.app_name), "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private String getAlbumName() {
        return getString(R.string.album_name);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //success on take picture
        switch (requestCode){
            case REQUEST_TAKE_PHOTO:
                if(resultCode == RESULT_OK) {
                    galleryAddPic();
                    callCaptureNotificationActivity();
                }else{
                    finish();
                }
                break;

            case REQUEST_CAPTURE:
                if(resultCode == RESULT_OK){
                    Intent ret = new Intent(data);
                    ret.putExtra(SendPoiIntentService.PICTURE, mCurrentPhotoPath);
                    setResult(RESULT_OK, ret);
                }
                finish();

            default:
                finish();
        }

    }



    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    //Method that call the Capture Notification Activity
    public void callCaptureNotificationActivity(){
        Intent intent = new Intent(this, CaptureNotificationActivity.class);
        intent.putExtra(TakePicture.TAG, mCurrentPhotoPath);
        startActivityForResult(intent, REQUEST_CAPTURE);

    }


}
