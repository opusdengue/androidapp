package app.dengue.opus.les.br.denguereport.navigationdrawer.userprofile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by leonardo on 11/25/15.
 */
public class DownloadImageCoverUser extends AsyncTask<String, Void, Drawable>{
    private RelativeLayout layout;

    public DownloadImageCoverUser(RelativeLayout relativeLayout) {
        this.layout = relativeLayout;
    }

    protected Drawable doInBackground(String... sLink) {
        Drawable drawable = null;
        try {
            InputStream in = new URL(sLink[0]).openStream();
            drawable = Drawable.createFromStream(in, null);

        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        }

        return drawable;
    }

    protected void onPostExecute(Drawable result) {
        layout.setBackground(result);
    }
}
