package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import br.les.opus.dengue.sdk.domain.fields.Field;
import br.les.opus.dengue.sdk.domain.fields.FieldOption;
import br.les.opus.dengue.sdk.domain.fields.FieldType;

/**
 * Created by leonardo on 7/14/15.
 */
public class FieldParcelable extends Field implements Parcelable{
    private List<FieldOptionParcelable> optionsParcelable;

    public FieldParcelable(Field field){
        setId(field.getId());
        setName(field.getName());
        setHelpText(field.getHelpText());
        setRequired(field.getRequired());
        setType(field.getType());

        List<FieldOption> optionsList = field.getOptions();

        optionsParcelable = new ArrayList<>();
        if(optionsList != null) {
            copyFieldOptions(field.getOptions());
        }
    }

    private void copyFieldOptions(List<FieldOption> options) {
        for(FieldOption fieldOption: options){
            this.optionsParcelable.add(new FieldOptionParcelable(fieldOption));
        }
    }

    public List<FieldOptionParcelable> getFieldOptionsParcelable(){
        return this.optionsParcelable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getName());
        dest.writeString(getHelpText());
        dest.writeValue(getRequired());
        dest.writeSerializable(getType());
        dest.writeTypedList(optionsParcelable);

    }

    public static final Parcelable.Creator<FieldParcelable> CREATOR
            = new Parcelable.Creator<FieldParcelable>(){
        @Override
        public FieldParcelable createFromParcel(Parcel source) {
            return new FieldParcelable(source);
        }

        @Override
        public FieldParcelable[] newArray(int size) {
            return new FieldParcelable[size];
        }
    };

    private FieldParcelable(Parcel in) {
        setId(in.readLong());
        setName(in.readString());
        setHelpText(in.readString());
        setRequired((Boolean) in.readValue(null));
        setType((FieldType) in.readSerializable());

        optionsParcelable = new ArrayList<>();
        in.readTypedList(optionsParcelable, FieldOptionParcelable.CREATOR);
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("Fields:\n");

        for(FieldOptionParcelable fop: optionsParcelable){
            stringBuffer.append(fop + "\n");
        }
        return stringBuffer.toString();
    }
}
