package app.dengue.opus.les.br.denguereport.shownotification.asynctasks;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import app.dengue.opus.les.br.denguereport.DengueMainActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PictureParcelable;
import app.dengue.opus.les.br.denguereport.shownotification.PlotMarker;
import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.ShowPoiDetails;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.Picture;
import br.les.opus.dengue.sdk.domain.PoiEnvelope;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;
import br.les.opus.dengue.sdk.util.SdkURIBuilder;

/**
 * Created by leonardo on 7/2/15.
 */
public class AsyncTasktoGetPictures extends AsyncTask<List<Picture>, Void, List<Bitmap>> {
    private Activity activity;

    public AsyncTasktoGetPictures(Activity activity){
        onAttach(activity);
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }


    @Override
    protected List<Bitmap> doInBackground(List<Picture>... params) {
        SdkURIBuilder sdkURIBuilder = new SdkURIBuilder();
        sdkURIBuilder.setPathPoi(SdkURIBuilder.PICTUREDOWNLOAD);

        String urlString = sdkURIBuilder.getUrlString();


        urlString = urlString.replace("{width}", "100");
        urlString = urlString.replace("{height}", "100");

        ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();

        Bitmap bitmap;
        try {

            for(Picture picture: params[0]) {
                URI uri = new URL(urlString.replace("{id}", Long.toString(picture.getId()))).toURI();

                InputStream in = uri.toURL().openStream();
                bitmap = BitmapFactory.decodeStream(in);

                bitmaps.add(bitmap);
            }
        } catch (URISyntaxException | MalformedURLException e) {
            return bitmaps;
        } catch (IOException e) {
            return bitmaps;
        }


        return bitmaps;
    }

    @Override
    protected void onPostExecute(List<Bitmap> bitmaps) {
        ((ShowPoiDetails) activity).hideProgressBar();

        if(bitmaps == null){
            return;
        }

        if(activity != null){
            ((ShowPoiDetails) activity).showImages(bitmaps);
        }

    }

}
