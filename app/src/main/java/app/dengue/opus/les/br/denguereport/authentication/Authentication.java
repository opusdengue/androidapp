package app.dengue.opus.les.br.denguereport.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.util.AlertDialogInterface;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;

public class Authentication extends AppCompatActivity {
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private FacebookUser facebookUser;
    private ProfileTracker profileTracker;
    private AccessTokenTracker accessTokenTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_authentication);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loginButton = (LoginButton) findViewById(R.id.login_button);

        //Callback registration - it handles with the login answer
        loginButton.registerCallback(callbackManager, new FacebookResponse());


        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.

                //user did the logout
                if(currentAccessToken == null){
                    facebookUser = new FacebookUser();
                    sendFacebookResult();
                }
            }
        };

        facebookUser = new FacebookUser();

        // If the access token is available already assign it.
        facebookUser.setAccessToken(AccessToken.getCurrentAccessToken());

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code
                facebookUser.setProfile(currentProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    //class that handles with the login response
    public class FacebookResponse implements FacebookCallback<LoginResult> {
        @Override
        public void onSuccess(LoginResult loginResult) {
            //Get the token and the user profile


            profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(
                        Profile oldProfile,
                        Profile currentProfile) {

                    facebookUser.setProfile(currentProfile);
                    facebookUser.setConnected(true);

                    GraphRequest request = GraphRequest.newMeRequest(
                            facebookUser.getAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {
                                    // Application code

                                    try {
                                        facebookUser.setUrlCover(object.getJSONObject("cover").getString("source"));
                                    }catch (Exception e){
                                    }

                                    sendFacebookResult();
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "cover");
                    request.setParameters(parameters);
                    request.executeAsync();

                }
            };

            accessTokenTracker.startTracking();
            profileTracker.startTracking();

            facebookUser.setAccessToken(loginResult.getAccessToken());



        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onError(FacebookException error) {

        }
    }

    //send to the main activity the result of login or logout
    private void sendFacebookResult(){
        Intent intentResult = new Intent();
        intentResult.putExtra(AuthenticationUser.RESULT, facebookUser);
        setResult(RESULT_FIRST_USER, intentResult);
        finish();
    }
}
