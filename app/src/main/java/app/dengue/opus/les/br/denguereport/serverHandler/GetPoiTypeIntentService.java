package app.dengue.opus.les.br.denguereport.serverHandler;

import android.app.IntentService;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PoiTypeParcelable;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.PoiType;
import br.les.opus.dengue.sdk.endpoints.EndpointConsumer;
import br.les.opus.dengue.sdk.endpoints.PoiTypeConsumer;

/**
 * Created by leonardo on 6/4/15.
 */
public class GetPoiTypeIntentService extends IntentService{
    public final static String RESPONSE_STRING = "PoiTypeIntentService.POITYPERECEIVE";
    public final static String EXTRA = "PoiTypeIntentService.EXTRA";
    public final static String STATUS = "PoiTypeIntentService.CONNECTED";

    public GetPoiTypeIntentService(){
        super("GetPoiTypeIntentService");
    }

    //@Override
    protected void onHandleIntent(Intent intent) {
        PoiTypeConsumer consumer;

        if(Locale.getDefault().getLanguage().equals("en")){
            consumer = new PoiTypeConsumer(EndpointConsumer.ENGLISH);
        }else{
            consumer = new PoiTypeConsumer();
        }

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(GetPoiTypeIntentService.RESPONSE_STRING);

        try{
            List<PoiType> types = consumer.getAllPoiTypes();
            broadcastIntent.putParcelableArrayListExtra(GetPoiTypeIntentService.EXTRA, convertPoiTypeParcelable(types));
            broadcastIntent.putExtra(GetPoiTypeIntentService.STATUS, true);

        }catch (SdkDengueException e){
            broadcastIntent.putExtra(GetPoiTypeIntentService.STATUS, false);
        }

        sendBroadcast(broadcastIntent);

    }

    private ArrayList<PoiTypeParcelable> convertPoiTypeParcelable(List<PoiType> types){
        ArrayList<PoiTypeParcelable> parcelable = new ArrayList<>();

        for (PoiType type: types){
            parcelable.add(new PoiTypeParcelable(type));
        }

        return parcelable;

    }
}
