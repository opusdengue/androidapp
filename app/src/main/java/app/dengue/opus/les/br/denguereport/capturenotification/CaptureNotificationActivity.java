package app.dengue.opus.les.br.denguereport.capturenotification;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


import app.dengue.opus.les.br.denguereport.AboutAppActivity;
import app.dengue.opus.les.br.denguereport.capturenotification.camera.TakePicture;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldValueParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.VariableFieldDengueFocusFragment;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.VariableFieldDiagnosedDengueFragment;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.VariableFieldFragment;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.VariableFieldSuspectedDengueFragment;
import app.dengue.opus.les.br.denguereport.serverHandler.SendPoiIntentService;
import app.dengue.opus.les.br.denguereport.util.AlertDialogInterface;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PoiTypeParcelable;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.animation.FloatingHintEditText;
import app.dengue.opus.les.br.denguereport.serverHandler.GetPoiTypeIntentService;
import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;

/**
 * Created by leonardo on 5/6/15.
 */
public class CaptureNotificationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    private Toolbar toolbar;
    private Spinner spinner;
    private Button btSend;
    private FloatingHintEditText editTextTitle;
    private FloatingHintEditText editTextDesc;
    private PoiTypeReceiver receiver;
    private HashMap<String, PoiTypeParcelable> poiTypes = new HashMap<>();
    private VariableFieldsFragment fieldsFragment;

    private VariableFieldDengueFocusFragment dengueFocusFragment;
    private VariableFieldDiagnosedDengueFragment diagnosedDengueFragment;
    private VariableFieldSuspectedDengueFragment suspectedDengueFragment;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listenForPoiTypes();

        setContentView(R.layout.activity_capture_notification);

        // Creating The Toolbar and setting it as the Toolbar for the activity
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextTitle = (FloatingHintEditText) findViewById(R.id.editTextTitle);
        editTextDesc = (FloatingHintEditText) findViewById(R.id.editTextDescription);

        populateSpinnerTypeNotifications(false);

        btSend = (Button) findViewById(R.id.btSendNotification);
        btSend.setVisibility(View.INVISIBLE);

        createTimeToast();
    }


    private void createTimeToast(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            private Handler handler = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_search_types), Toast.LENGTH_SHORT).show();
                }

            };

            @Override
            public void run() {
                try {
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 2000, 5000);
    }


    private void populateSpinnerTypeNotifications(boolean inflateLayout){
        spinner = (Spinner) findViewById(R.id.spNotificationType);

        ArrayList<String> spinnerValues = new ArrayList<>();

        if(inflateLayout){
            dengueFocusFragment = new VariableFieldDengueFocusFragment();
            diagnosedDengueFragment = new VariableFieldDiagnosedDengueFragment();
            suspectedDengueFragment = new VariableFieldSuspectedDengueFragment();

            spinnerValues = new ArrayList<>(poiTypes.keySet());
        }

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, spinnerValues);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner.setAdapter(typeAdapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        changeLayout((String) parent.getItemAtPosition(position));
    }

    private void changeLayout(String key){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        Bundle bundle = new Bundle();
        PoiTypeParcelable poiTypeParcelable = poiTypes.get(key);
        bundle.putParcelable(VariableFieldsFragment.EXTRA, poiTypeParcelable);

        fieldsFragment = new VariableFieldsFragment();
        fieldsFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.fragment_container, fieldsFragment);

        /*if (item.equals("Pessoa com suspeita de dengue") || item.equals("Person with dengue or suspected")){
            suspectedDengueFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.fragment_container, suspectedDengueFragment);
        }else{
            if (item.equals("Pessoa com dengue diagnosticada") || item.equals("Person diagnosed with dengue")){
                diagnosedDengueFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.fragment_container, diagnosedDengueFragment);
            }else{
                if (item.equals("Foco do mosquito dengue") || item.equals("Dengue mosquito focus")){
                    dengueFocusFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.fragment_container, dengueFocusFragment);
                }
            }
        }*/


        fragmentTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.menu_dengue_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, AboutAppActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

     @Override
     public void onDestroy(){
         this.unregisterReceiver(receiver);
         timer.cancel();
         super.onDestroy();
     }


    public void listenForPoiTypes(){
        Intent ptIntent = new Intent(this, GetPoiTypeIntentService.class);
        startService(ptIntent);

        IntentFilter filter = new IntentFilter(GetPoiTypeIntentService.RESPONSE_STRING);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new PoiTypeReceiver();
        registerReceiver(receiver, filter);
    }

    public void sendPoiToServer(View v){
        if (areEditTextsEmpty()){
            return;
        }

        Intent ret = new Intent();

        String title = editTextTitle.getText().toString();
        String description = editTextDesc.getText().toString();

        PoiTypeParcelable poiType = new PoiTypeParcelable();
        poiType.setName(spinner.getSelectedItem().toString());

        poiType.setId(poiTypes.get(poiType.getName()).getId());

        TemporaryPoi poi = new TemporaryPoi(title, description, poiType);

        String item = (String)spinner.getSelectedItem();

        poi.setFieldValueParcelables(fieldsFragment.getFields());


        /*if (item.equals("Pessoa com suspeita de dengue") || item.equals("Person with dengue or suspected")){
            poi.setFieldValueParcelables(suspectedDengueFragment.getFields());

        }else{
            if (item.equals("Pessoa com dengue diagnosticada") || item.equals("Person diagnosed with dengue")){
                poi.setFieldValueParcelables(diagnosedDengueFragment.getFields());
            }else{
                if (item.equals("Foco do mosquito dengue") || item.equals("Dengue mosquito focus")){
                    poi.setFieldValueParcelables(dengueFocusFragment.getFields());
                }
            }
        }*/

        ret.putExtra(SendPoiIntentService.POI, poi);

        setResult(RESULT_OK, ret);

        finish();
    }

    public boolean areEditTextsEmpty(){
        if(editTextTitle.getText().toString().trim().length() == 0){
            Toast.makeText(getApplicationContext(), getString(R.string.toast_title_empty), Toast.LENGTH_SHORT).show();
            return true;
        }

        if(editTextDesc.getText().toString().trim().length() == 0){
            Toast.makeText(getApplicationContext(), getString(R.string.toast_description_empty), Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    //Service to connect the server and get the different types of poi
    public class PoiTypeReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent){
            boolean status = intent.getBooleanExtra(GetPoiTypeIntentService.STATUS, false);

            if(status){
                timer.cancel();

                ArrayList<PoiTypeParcelable> types = intent.getParcelableArrayListExtra(GetPoiTypeIntentService.EXTRA);

                for(PoiTypeParcelable ptp: types){
                    poiTypes.put(ptp.getName(), ptp);
                }

                populateSpinnerTypeNotifications(true);

                btSend.setVisibility(View.VISIBLE);
            }else{
                timer.cancel();
                DialogAlert.displayDialog(
                        context,
                        getString(R.string.error_message),
                        getString(R.string.server_unreacheable),
                        getString(R.string.yes_button),
                        getString(R.string.cancel_button),
                        true,
                        new AlertDialogInterface() {
                            @Override
                            public void onButtonClicked(boolean value) {
                                if(value){
                                    unregisterReceiver(receiver);
                                    createTimeToast();
                                    listenForPoiTypes();
                                }else{
                                    finish();
                                }
                            }
                        });
            }

        }
    }
}
