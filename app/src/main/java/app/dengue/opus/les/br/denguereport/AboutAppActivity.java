package app.dengue.opus.les.br.denguereport;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;

import app.dengue.opus.les.br.denguereport.shownotification.markerdetails.MarkerDetails;
import app.dengue.opus.les.br.denguereport.util.InstagramParcelable;
import br.les.opus.dengue.sdk.domain.PointOfInterest;

/**
 * Created by leonardo on 10/12/15.
 */
public class AboutAppActivity extends AppCompatActivity{
    public static final String WEBSITE = "http://vazadengue.inf.puc-rio.br/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.about_app);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void goToSite(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(WEBSITE));
        startActivity(browserIntent);
    }
}
