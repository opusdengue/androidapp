package app.dengue.opus.les.br.denguereport.shownotification.asynctasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Timer;

import app.dengue.opus.les.br.denguereport.DengueMainActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.shownotification.PlotMarker;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.TweetEnvelope;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;

/**
 * Created by leonardo on 7/2/15.
 */
public class AsyncTasktoGetTweets extends AsyncTask<Void, Void, TweetEnvelope> {
    private Activity activity;
    private PlotMarker plotMarker;
    private Timer timer;

    public AsyncTasktoGetTweets(Activity activity, Timer timer){
        this.timer = timer;
        onAttach(activity);
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }

    @Override
    protected TweetEnvelope doInBackground(Void... params){
        PoiConsumer consumer = new PoiConsumer();

        try {
            return consumer.getAllTweets();
        }catch (SdkDengueException e){
            return null;
        }

    }

    @Override
    protected void onPostExecute(TweetEnvelope tweetEnvelope) {
        if(tweetEnvelope == null){
            timer.cancel();
            if(activity != null) {
                ((DengueMainActivity) activity).displayErrorInfo();
            }
            return;
        }

        if(activity != null) {
            timer.cancel();
            plotMarker = new PlotMarker(
                    ((DengueMainActivity) activity).getMap(),
                    activity
            );

            //after receiving the tweets, this method plotPointOfInterests them on screen
            plotMarker.plotTweets(tweetEnvelope);

            String sToast = activity.getResources().getString(R.string.toast_show_number_tweets);

            sToast = String.format(sToast, tweetEnvelope.getContent().size());

            Toast.makeText(activity, sToast, Toast.LENGTH_SHORT).show();
        }
    }

}
