package app.dengue.opus.les.br.denguereport.util;

import android.os.Bundle;

import com.google.android.gms.maps.MapFragment;

/**
 * Created by leonardo on 7/2/15.
 */
public class RetainMapFragment extends MapFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }
}
