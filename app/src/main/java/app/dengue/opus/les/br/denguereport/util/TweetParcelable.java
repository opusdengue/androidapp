package app.dengue.opus.les.br.denguereport.util;

import android.os.Parcel;
import android.os.Parcelable;


import java.util.Date;

import br.les.opus.dengue.sdk.domain.Tweet;
import br.les.opus.dengue.sdk.domain.TweetClassification;
import br.les.opus.dengue.sdk.domain.TwitterUser;


/**
 * Created by leonardo on 10/10/15.
 *
 * Parcelable class that will store a tweet
 */
public class TweetParcelable implements Parcelable{
    private String text;
    private Date createdAt;
    private TweetUser user;
    private Classification classification;

    public TweetParcelable(Tweet tweet) {
        this.text = tweet.getText();
        this.createdAt = tweet.getCreatedAt();
        this.classification = new Classification(tweet.getClassification());
        this.user = new TweetUser(tweet.getUser());
    }

    public String getText() {
        return text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Classification getClassification() {
        return classification;
    }

    public TweetUser getUser(){
        return user;
    }

    //everything bellow is related to parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeLong(createdAt.getTime());

        dest.writeParcelable(user, flags);
        dest.writeParcelable(classification, flags);
    }

    public static final Parcelable.Creator<TweetParcelable> CREATOR
            = new Parcelable.Creator<TweetParcelable>(){
        @Override
        public TweetParcelable createFromParcel(Parcel source) {
            return new TweetParcelable(source);
        }

        @Override
        public TweetParcelable[] newArray(int size) {
            return new TweetParcelable[size];
        }
    };

    private TweetParcelable(Parcel in){
        this.text = in.readString();
        this.createdAt = new Date(in.readLong());
        this.user = in.readParcelable(TweetUser.class.getClassLoader());
        this.classification = in.readParcelable(Classification.class.getClassLoader());
    }

    //Inner class to store the tweet User
    public static class TweetUser implements Parcelable{
        private String user;
        private String profile;
        private String urlProfile;

        public TweetUser(TwitterUser tweetUser) {
            this.user = tweetUser.getName();
            this.profile = tweetUser.getScreenName();
            this.urlProfile = tweetUser.getProfileImageUrl();
        }

        public String getUser() {
            return user;
        }

        public String getProfile() {
            return profile;
        }

        public String getUrlProfile(){
            return urlProfile;
        }

        //everything bellow is related to the parcelable
        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(user);
            dest.writeString(profile);
            dest.writeString(urlProfile);
        }

        public static final Parcelable.Creator<TweetUser> CREATOR
                = new Parcelable.Creator<TweetUser>(){
            @Override
            public TweetUser createFromParcel(Parcel source) {
                return new TweetUser(source);
            }

            @Override
            public TweetUser[] newArray(int size) {
                return new TweetUser[size];
            }
        };

        private TweetUser (Parcel in){
            user = in.readString();
            profile = in.readString();
            urlProfile = in.readString();
        }
    }

    //Inner class to store the tweet classification
    public static class Classification implements Parcelable{
        private String classification;
        private String description;

        public Classification(TweetClassification tweetClassification) {
            this.classification = tweetClassification.getKey();
            this.description = tweetClassification.getLabel();
        }

        public String getClassification() {
            return classification;
        }

        public String getDescription() {
            return description;
        }

        //everything bellow is related to the parcelable
        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(classification);
            dest.writeString(description);
        }

        public static final Parcelable.Creator<Classification> CREATOR
                = new Parcelable.Creator<Classification>(){
            @Override
            public Classification createFromParcel(Parcel source) {
                return new Classification(source);
            }

            @Override
            public Classification[] newArray(int size) {
                return new Classification[size];
            }
        };

        private Classification (Parcel in){
            classification = in.readString();
            description = in.readString();
        }
    }
}
