package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.animation.FloatingHintEditText;

/**
 * Created by leonardo on 7/21/15.
 */
public class VariableFieldSuspectedDengueFragment extends VariableFieldFragment {
    private ArrayList<TextView> arrayViews;
    private ArrayList<Spinner> arraySpinners;
    private FloatingHintEditText editText;
    List<FieldParcelable> fieldParcelable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);

        Bundle bundleArg = getArguments();

        PoiTypeParcelable poiTypeParcelable = bundleArg.getParcelable(EXTRA);

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.variable_field_suspected_dengue, container, false);

        inflateWidgets(rootView, poiTypeParcelable);

        return rootView;
    }

    @Override
    public void inflateWidgets(View rootView, PoiTypeParcelable poiTypeParcelable){
        arrayViews = new ArrayList<>(20);
        arraySpinners = new ArrayList<>(19);

        fieldParcelable = poiTypeParcelable.getFieldParcelable();

        arrayViews.add((TextView) rootView.findViewById(R.id.textview1));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview2));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview3));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview4));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview5));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview6));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview7));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview8));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview9));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview10));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview11));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview12));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview13));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview14));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview15));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview16));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview17));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview18));
        arrayViews.add((TextView) rootView.findViewById(R.id.textview19));

        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner1));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner2));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner3));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner4));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner5));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner6));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner7));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner8));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner9));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner10));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner11));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner12));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner13));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner14));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner15));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner16));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner17));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner18));
        arraySpinners.add((Spinner) rootView.findViewById(R.id.spinner19));


        for(int i = 0; i < arraySpinners.size(); i++){
            arrayViews.get(i).setText(fieldParcelable.get(i).getName());
            populateSpinnerTypeNotifications(arraySpinners.get(i), fieldParcelable.get(i).getFieldOptionsParcelable());
        }

        editText = (FloatingHintEditText) rootView.findViewById(R.id.editTextAge);
        editText.setHint(fieldParcelable.get(fieldParcelable.size() - 1).getName());
    }

    @Override
    public void populateSpinnerTypeNotifications(Spinner spinner, List<FieldOptionParcelable> fieldOptions){

        ArrayList<String> spinnerValues = new ArrayList<>();

        for(FieldOptionParcelable option: fieldOptions){
            spinnerValues.add(option.getValue());
        }

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(
                this.getActivity(), android.R.layout.simple_spinner_item, spinnerValues);

        typeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner.setAdapter(typeAdapter);
    }

    @Override
    public List<FieldValueParcelable> getFields() {
        ArrayList<FieldValueParcelable> arrayFieldValues = new ArrayList<>();

        for(int i = 0; i < arraySpinners.size(); i++){
            FieldValueParcelable fieldValueParcelable = new FieldValueParcelable(
                    (String)arraySpinners.get(i).getSelectedItem(),
                    fieldParcelable.get(i).getId(),
                    fieldParcelable.get(i).getName()
            );
            arrayFieldValues.add(fieldValueParcelable);
        }

        FieldValueParcelable fieldValueParcelable = new FieldValueParcelable(
                editText.getText().toString(),
                fieldParcelable.get(fieldParcelable.size() - 1).getId(),
                fieldParcelable.get(fieldParcelable.size() - 1).getName());

        arrayFieldValues.add(fieldValueParcelable);

        return arrayFieldValues;
    }
}
