package app.dengue.opus.les.br.denguereport.authentication;

/**
 * Created by leonardo on 11/24/15.
 */
public class AuthenticationUser {
    public static final String RESULT = "authentication.RESULT";     //constant to identify the class
    public static final String LOGOUT = "authentication.LOGOUT";    //constant to indicate the logout

    public AuthenticationUser() {
    }

}
