package app.dengue.opus.les.br.denguereport.shownotification.asynctasks;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import app.dengue.opus.les.br.denguereport.DengueMainActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.shownotification.PlotMarker;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.PointOfInterest;
import br.les.opus.dengue.sdk.domain.Tweet;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;

/**
 * Created by leonardo on 7/2/15.
 */
public class AsyncTasktoGetHeatmap extends AsyncTask<Void, Void, List<LatLng>> {
    private Activity activity;
    private PlotMarker plotMarker;
    private Timer timer;

    public AsyncTasktoGetHeatmap(Activity activity, Timer timer){
        this.timer = timer;
        onAttach(activity);
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }

    @Override
    protected List<LatLng> doInBackground(Void... params) {
        List<LatLng> pois = new ArrayList<>();


        PoiConsumer consumer = new PoiConsumer();

        try {

            for (PointOfInterest poi : consumer.getAllPointsOfInterest().getContent()) {
                pois.add(new LatLng(poi.getLocation().getLat(), poi.getLocation().getLng()));
            }


            for (Tweet tweet : consumer.getAllTweets().getContent()) {
                pois.add(new LatLng(tweet.getGeolocation().getLat(), tweet.getGeolocation().getLng()));
            }
            return pois;
        }catch (SdkDengueException e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<LatLng> tweets) {
        if(tweets == null){
            timer.cancel();
            if(activity != null) {
                ((DengueMainActivity) activity).displayErrorInfo();
            }
            return;
        }
        if(activity != null){
            timer.cancel();
            plotMarker = new PlotMarker(
                    ((DengueMainActivity)activity).getMap(),
                    activity
            );
            plotMarker.plotHitMap(tweets);
        }

    }

}
