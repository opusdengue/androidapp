package app.dengue.opus.les.br.denguereport.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by leonardo on 6/12/15.
 */
public class DialogAlert {
    //regular dialog for cancel or ok operations
    public static void displayDialog(final Context mContext,
                                        final String title, final String msg,
                                        final String positiveBtnCaption, final String negativeBtnCaption,
                                        final boolean isCancelable, final AlertDialogInterface target) {

        ((Activity) mContext).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setTitle(title)
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(positiveBtnCaption,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        target.onButtonClicked(true);
                                    }
                                })
                        .setNegativeButton(negativeBtnCaption,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        target.onButtonClicked(false);
                                    }
                                });

                AlertDialog alert = builder.create();
                alert.setCancelable(isCancelable);
                alert.show();
                if (isCancelable) {
                    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface arg0) {
                            target.onButtonClicked(false);
                        }
                    });
                }
            }
        });

    }

    //regular dialog for display a message
    public static void displayInfoDialog(final Context mContext,
                                         final String title, final String msg,
                                         final String button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        builder.show();
    }
}
