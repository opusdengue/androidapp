package app.dengue.opus.les.br.denguereport.capturenotification;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.animation.FloatingHintEditText;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldOptionParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.FieldValueParcelable;
import app.dengue.opus.les.br.denguereport.capturenotification.poitypefield.PoiTypeParcelable;

/**
 * Created by leonardo on 12/9/15.
 */
@TargetApi(21)
public class VariableFieldsFragment extends Fragment {
    private List<FieldParcelable> fieldParcelableList;
    private List<ContainerFieldView> fieldViews;
    public static final String EXTRA = "VariableFieldsFragment.EXTRA";
    private String select = "Select";
    private String number = "Number";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);

        Bundle bundleArg = getArguments();

        ScrollView scrollView = new ScrollView(getActivity());

        PoiTypeParcelable poiTypeParcelable = bundleArg.getParcelable(EXTRA);

        fieldParcelableList = poiTypeParcelable.getFieldParcelable();

        inflateWidgets(scrollView);

        return scrollView;
    }

    public void inflateWidgets(ScrollView rootView){
        fieldViews = new ArrayList<>();

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        ContainerFieldView containerFieldView;

        for(FieldParcelable fieldParcelable: fieldParcelableList){
            if(fieldParcelable.getType().getName().equals(select)){
                TextView textView = new TextView(getActivity());
                textView.setText(fieldParcelable.getName());


                Spinner spinner = new Spinner(getActivity());
                spinner.setBackground(getActivity().getDrawable(R.drawable.spinner_triangle));

                containerFieldView = new ContainerFieldView(spinner, fieldParcelable);
                containerFieldView.populateSpinnerTypeNotifications(getActivity(),fieldParcelable);
                fieldViews.add(containerFieldView);

                layout.addView(textView);
                layout.addView(spinner);
            }else{
                FloatingHintEditText editText = new FloatingHintEditText(getActivity());
                editText.setTextAppearance(getActivity().getApplicationContext(), R.style.EditTextForm);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText.setTextColor(getResources().getColor(R.color.secondary_text));
                editText.setHint(fieldParcelable.getName());
                editText.setHintTextColor(getResources().getColor(R.color.primary));

                containerFieldView = new ContainerFieldView(editText, fieldParcelable);
                fieldViews.add(containerFieldView);

                layout.addView(editText);
            }

        }

        rootView.addView(layout);

    }

    public List<FieldValueParcelable> getFields() {
        ArrayList<FieldValueParcelable> arrayFieldValues = new ArrayList<>();

        for(ContainerFieldView container: fieldViews){
            FieldParcelable fieldParcelable = container.getFieldParcelable();

            if(container.getSpinner() != null){
                arrayFieldValues.add(new FieldValueParcelable(
                        (String)container.getSpinnerValue((String)container.getSpinner().getSelectedItem()),
                        fieldParcelable.getId(),
                        fieldParcelable.getName()
                ));
            }else{
                arrayFieldValues.add(new FieldValueParcelable(
                        container.getEditText(),
                        fieldParcelable.getId(),
                        fieldParcelable.getName()
                ));
            }
        }

        return arrayFieldValues;
    }
}
