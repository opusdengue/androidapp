package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.os.Parcel;
import android.os.Parcelable;

import br.les.opus.dengue.sdk.domain.fields.FieldOption;

/**
 * Created by leonardo on 7/14/15.
 */
public class FieldOptionParcelable extends FieldOption implements Parcelable{

    public FieldOptionParcelable(FieldOption fieldOption){
        setId(fieldOption.getId());
        setLabel(fieldOption.getLabel());
        setValue(fieldOption.getValue());
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getLabel());
        dest.writeString(getValue());
    }

    public static final Parcelable.Creator<FieldOptionParcelable> CREATOR
            = new Parcelable.Creator<FieldOptionParcelable>(){
        @Override
        public FieldOptionParcelable createFromParcel(Parcel source) {
            return new FieldOptionParcelable(source);
        }

        @Override
        public FieldOptionParcelable[] newArray(int size) {
            return new FieldOptionParcelable[size];
        }
    };

    private FieldOptionParcelable(Parcel in){
        setId(in.readLong());
        setLabel(in.readString());
        setValue(in.readString());
    }

    @Override
    public String toString() {
        return "label: " + getLabel() + " value: " + getValue();
    }
}
