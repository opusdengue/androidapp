package app.dengue.opus.les.br.denguereport.shownotification.markerdetails;

import br.les.opus.dengue.sdk.domain.PointOfInterest;

/**
 * Created by leonardo on 10/13/15
 *
 * Classes that will show markers info should implement this interface.
 * This interface is used to standardize the marker details
 */
public interface MarkerDetails {
    String EXTRA = "shownotification.markerdetails.MarkerDetails.EXTRA";

    void addUIComponents(PointOfInterest poi);
}
