package app.dengue.opus.les.br.denguereport.shownotification.asynctasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Timer;

import app.dengue.opus.les.br.denguereport.DengueMainActivity;
import app.dengue.opus.les.br.denguereport.R;
import app.dengue.opus.les.br.denguereport.shownotification.PlotMarker;
import app.dengue.opus.les.br.denguereport.util.DialogAlert;
import br.les.opus.dengue.sdk.api.SdkDengueException;
import br.les.opus.dengue.sdk.domain.PoiEnvelope;
import br.les.opus.dengue.sdk.endpoints.PoiConsumer;

/**
 * Created by leonardo on 7/2/15.
 */
public class AsyncTasktoGetPois extends AsyncTask<Void, Void, PoiEnvelope> {
    private Activity activity;
    private PlotMarker plotMarker;
    private Timer timer;

    public AsyncTasktoGetPois(Activity activity, Timer timer){
        onAttach(activity);
        this.timer = timer;
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }

    @Override
    protected PoiEnvelope doInBackground(Void... params) {
        PoiConsumer consumer = new PoiConsumer();

        try {
            return consumer.getAllPointsOfInterest();
        }catch (SdkDengueException e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(PoiEnvelope poiEnvelope) {
        if(poiEnvelope == null){
            timer.cancel();
            if(activity != null) {
                ((DengueMainActivity) activity).displayErrorInfo();
            }
            return;
        }

        if(activity != null){
            timer.cancel();

            plotMarker = new PlotMarker(
                    ((DengueMainActivity)activity).getMap(),
                    activity
            );
            plotMarker.plotPointOfInterests(poiEnvelope);

            String sToast = activity.getResources().getString(R.string.toast_show_number_notifications);

            sToast = String.format(sToast, poiEnvelope.getContent().size());

            Toast.makeText(activity, sToast, Toast.LENGTH_SHORT).show();
        }

    }

}
