package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.os.Parcel;
import android.os.Parcelable;

import app.dengue.opus.les.br.denguereport.util.TemporaryPoi;

/**
 * Created by leonardo on 12/7/15.
 */
public class PictureParcelable implements Parcelable{
    private long id;
    private String localUrl;

    public PictureParcelable(String localUrl) {
        this.id = 0;
        this.localUrl = localUrl;
    }

    public PictureParcelable(long id) {
        this.id = id;
        localUrl = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getLocalUrl());
    }

    public static final Parcelable.Creator<PictureParcelable> CREATOR
            = new Parcelable.Creator<PictureParcelable>(){
        @Override
        public PictureParcelable createFromParcel(Parcel source) {
            return new PictureParcelable(source);
        }

        @Override
        public PictureParcelable[] newArray(int size) {
            return new PictureParcelable[size];
        }
    };

    private PictureParcelable(Parcel in){
        setId(in.readLong());
        setLocalUrl(in.readString());
    }
}
