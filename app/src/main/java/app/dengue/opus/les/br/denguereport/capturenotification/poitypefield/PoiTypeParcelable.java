package app.dengue.opus.les.br.denguereport.capturenotification.poitypefield;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import br.les.opus.dengue.sdk.domain.PoiType;
import br.les.opus.dengue.sdk.domain.fields.Field;

/**
 * Created by leonardo on 6/12/15.
 */
public class PoiTypeParcelable extends PoiType implements Parcelable {
    private List<FieldParcelable> fieldsParcelable;

    public PoiTypeParcelable(){}

    public PoiTypeParcelable(PoiType type){
        setId(type.getId());
        setName(type.getName());

        fieldsParcelable = new ArrayList<>();

         /*
         * The fields can be null when the poi is sent to the server, because is not necessary send
         * all the information about the poi, only the information that will be stored in the server
        */
        if(type.getFields() != null) {
            copyFieldValuess(type.getFields());
        }
    }

    public List<FieldParcelable> getFieldParcelable() {
        return fieldsParcelable;
    }

    private void copyFieldValuess(List<Field> options) {
        for(Field field: options){
            this.fieldsParcelable.add(new FieldParcelable(field));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(getName());
        dest.writeTypedList(fieldsParcelable);
    }

    public static final Parcelable.Creator<PoiTypeParcelable> CREATOR
            = new Parcelable.Creator<PoiTypeParcelable>(){

        @Override
        public PoiTypeParcelable createFromParcel(Parcel source) {
            return new PoiTypeParcelable(source);
        }

        @Override
        public PoiTypeParcelable[] newArray(int size) {
            return new PoiTypeParcelable[size];
        }
    };

    private PoiTypeParcelable(Parcel in){
        setId(in.readLong());
        setName(in.readString());
        fieldsParcelable = new ArrayList<>();
        in.readTypedList(fieldsParcelable, FieldParcelable.CREATOR);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        for (Field f: fieldsParcelable){
            buffer.append("\n" + f);
        }
        return "id: " + getId() + " name: " + getName() + " fields: " + buffer;
    }

}
